﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class DisplayFactory
    {
        public static DisplayProperties GetDisplay(RLTile tile, DisplayProperties display, bool entityRender = false, bool ignoreHighlights = false)
        {
            DisplayProperties blendedDisplay = display;

            //Not explored and not debugging
            //Hide
            if (!tile.Explored && !MapSystem.DebugRendering())
            {
                blendedDisplay.Color = ColorLibrary.Colors.UIBase;
                blendedDisplay.Background = ColorLibrary.Colors.UIBase;
                return blendedDisplay;
            }

            //Not ignoring highlights and is highlighted
            //Highlight
            if(!ignoreHighlights && (tile.Highlighted || tile.Hovered))
            {
                blendedDisplay.Color = ColorLibrary.Colors.Hover;
                blendedDisplay.Background = ColorLibrary.Colors.HoverB;
                return blendedDisplay;
            }

            //In view and occupied
            if(tile.InFOV && entityRender)
            {
                blendedDisplay = display;
                if(display.Background == ColorLibrary.Colors.Null)
                {
                    blendedDisplay.Background = tile.DefaultDisplay.Background;
                }
                return blendedDisplay;
            }

            //Not in view and occupied but debugging
            if (!tile.InFOV && entityRender && MapSystem.DebugRendering())
            {
                blendedDisplay = display;
                if (display.Background == ColorLibrary.Colors.Null)
                {
                    blendedDisplay.Background = tile.DefaultDisplay.Background;
                }
                return blendedDisplay;
            }

            switch (tile.TType)
            {
                case RLTile.TileType.Ground:
                    blendedDisplay.Color = tile.InFOV ? ColorLibrary.Colors.Ground_Light : ColorLibrary.Colors.Ground_Dark;
                    blendedDisplay.Background = ColorLibrary.Colors.UIBase;
                    break;
                case RLTile.TileType.Wall:
                    blendedDisplay.Color = tile.InFOV ? ColorLibrary.Colors.Wall_Light : ColorLibrary.Colors.Wall_Dark;
                    blendedDisplay.Background = tile.InFOV ? ColorLibrary.Colors.Wall_LightB : ColorLibrary.Colors.Wall_DarkB;
                    break;
                case RLTile.TileType.Stairs:
                    display.Color = tile.InFOV ? ColorLibrary.Colors.Wall_Stairs_Up : ColorLibrary.Colors.Wall_Stairs_Up;
                    display.Background = ColorLibrary.Colors.UIBase;
                    break;
                case RLTile.TileType.NotSet:
                    break;
            }

            return blendedDisplay;
        }

        public static void InitializeDisplay(RLTile tile)
        {
            tile.DefaultDisplay.Color = ColorLibrary.Colors.UIBase;
            tile.DefaultDisplay.Background = ColorLibrary.Colors.UIBase;

            switch (tile.TType)
            {
                case RLTile.TileType.Ground:
                    tile.DefaultDisplay.Representation = CharacterLibrary.Characters.EnvironmentSettings.Ground;
                    tile.DefaultDisplay.Color = ColorLibrary.Colors.Ground_Dark;
                    break;
                case RLTile.TileType.Wall:
                    tile.DefaultDisplay.Representation = CharacterLibrary.Characters.EnvironmentSettings.Wall;
                    tile.DefaultDisplay.Color = ColorLibrary.Colors.Wall_Dark;
                    break;
                case RLTile.TileType.NotSet:
                    break;
            }

            tile.NeedsUpdate = true;
        }
    }
}
