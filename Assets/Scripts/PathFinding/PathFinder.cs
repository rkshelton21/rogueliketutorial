﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using RogueSharp.Algorithms;

namespace RogueSharp
{
    /// <summary>
    /// A class which can be used to find shortest path from a source to a destination in a Map
    /// </summary>
    public class PathFinder
    {
        private readonly EdgeWeightedDigraph _graph;
        
        /// <summary>
        /// Constructs a new PathFinder instance for the specified Map that will consider diagonal movement by using the specified diagonalCost
        /// </summary>
        /// <param name="map">The Map that this PathFinder instance will run shortest path algorithms on</param>
        /// <param name="diagonalCost">
        /// The cost of diagonal movement compared to horizontal or vertical movement. 
        /// Use 1.0 if you want the same cost for all movements.
        /// On a standard cartesian map, it should be sqrt(2) (1.41)
        /// </param>
        /// <exception cref="ArgumentNullException">Thrown when a null map parameter is passed in</exception>
        public PathFinder(LevelData level, double diagonalCost)
        {
            _graph = new EdgeWeightedDigraph(Constants.MapWidth * Constants.MapHeight);
            foreach (RLTile cell in level.GetAllCells())
            {
                if (cell.Walkable)
                {
                    int v = IndexFor(cell);
                    foreach (RLTile neighbor in level.GetBorderCellsInSquare(cell.X, cell.Y, 1))
                    {
                        if (neighbor.Walkable)
                        {
                            int w = IndexFor(neighbor);
                            if (neighbor.X != cell.X && neighbor.Y != cell.Y)
                            {
                                _graph.AddEdge(new DirectedEdge(v, w, diagonalCost));
                                _graph.AddEdge(new DirectedEdge(w, v, diagonalCost));
                            }
                            else
                            {
                                _graph.AddEdge(new DirectedEdge(v, w, 1.0));
                                _graph.AddEdge(new DirectedEdge(w, v, 1.0));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns a shortest Path containing a list of Cells from a specified source Cell to a destination Cell
        /// </summary>
        /// <param name="source">The Cell which is at the start of the path</param>
        /// <param name="destination">The Cell which is at the end of the path</param>
        /// <exception cref="ArgumentNullException">Thrown when source or destination is null</exception>
        /// <exception cref="PathNotFoundException">Thrown when there is not a path from the source to the destination</exception>
        /// <returns>Returns a shortest Path containing a list of Cells from a specified source Cell to a destination Cell</returns>
        public Path ShortestPath(RLTile source, RLTile destination, int depth)
        {
            Path shortestPath = TryFindShortestPath(source, destination, depth);

            if (shortestPath == null)
            {
                throw new PathNotFoundException(string.Format("Path from ({0}, {1}) to ({2}, {3}) not found", source.X, source.Y, destination.X, destination.Y));
            }

            return shortestPath;
        }

        /// <summary>
        /// Returns a shortest Path containing a list of Cells from a specified source Cell to a destination Cell
        /// </summary>
        /// <param name="source">The Cell which is at the start of the path</param>
        /// <param name="destination">The Cell which is at the end of the path</param>
        /// <exception cref="ArgumentNullException">Thrown when source or destination is null</exception>
        /// <returns>Returns a shortest Path containing a list of Cells from a specified source Cell to a destination Cell. If no path is found null will be returned</returns>
        public Path TryFindShortestPath(RLTile source, RLTile destination, int depth)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (destination == null)
            {
                throw new ArgumentNullException("destination");
            }

            var cells = ShortestPathCells(source, destination, depth).ToList();
            if (cells[0] == null)
            {
                return null;
            }
            return new Path(cells);
        }

        private IEnumerable<RLTile> ShortestPathCells(RLTile source, RLTile destination, int depth)
        {
            IEnumerable<DirectedEdge> path = DijkstraShortestPath.FindPath(_graph, IndexFor(source), IndexFor(destination));
            if (path == null)
            {
                yield return null;
            }
            else
            {
                yield return source;
                foreach (DirectedEdge edge in path)
                {
                    yield return CellFor(edge.To, depth);
                }
            }
        }

        private int IndexFor(RLTile cell)
        {
            return (cell.Y * Constants.MapWidth) + cell.X;
        }

        private RLTile CellFor(int index, int depth)
        {
            int x = index % Constants.MapWidth;
            int y = index / Constants.MapWidth;

            return MapSystem.Tile(x, y, depth);
        }
    }
}
