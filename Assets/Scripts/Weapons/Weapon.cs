﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    [Serializable]
    public class Weapon : ModifierItemBase
    {
        protected Constants.WeaponType _WeaponType;

        public Weapon()
        {
        }

        protected virtual Entity CreateEntityBase()
        {
            var entity = new Entity(CharacterLibrary.Characters.EntitySettings.Weapon);
            entity.Occupies = false;
            entity.RenderOrder = 20;
            entity.Display.Color = ColorLibrary.Colors.Potion;
            return entity;
        }

        public virtual Entity SpawnWeapon(Vector2Int position)
        {
            var newEntity = CreateEntityBase();
            newEntity.GridPosition = position;
            AddComponents(newEntity, _WeaponType);

            newEntity.Name = _WeaponType.ToString();
            newEntity.Description = this.Description;
            return newEntity;
        }

        public virtual void AddComponents(Entity entity, Constants.WeaponType weaponType)
        {
            entity.AddComonent(this);
            entity.AddComonent(new ItemComponent(CharacterLibrary.Characters.EntitySettings.Weapon) { });
        }

        protected override MessageType GetRequiredAction(AttributeModifier modifier, Entity user, Entity target)
        {
            if (IsPlayer(user) && IsPlayer(target))
            {
                return MessageType.PlayerModified;
            }
            else
            {
                return MessageType.None;
            }
        }

        public override bool Apply(AttributeModifier modifier, Entity user, Entity target)
        {
            var item = modifier.Owner.GetComponent<ItemComponent>(ComponentType.Item);
            if (item.Equipped)
            {
                var unequipped = user.GetComponent<InventoryComponent>(ComponentType.Inventory).UnEquip(item);
                if (unequipped)
                {
                    modifier.Used = false;
                    modifier.Ammount = -modifier.Ammount;
                    base.Apply(modifier, user, target);
                    modifier.Ammount = -modifier.Ammount;
                    item.Equipped = false;
                    modifier.Used = false;
                }
            }
            else
            {
                var equipped = user.GetComponent<InventoryComponent>(ComponentType.Inventory).Equip(item);
                if (equipped)
                {
                    base.Apply(modifier, user, target);
                    item.Equipped = true;
                    modifier.Used = false;
                }
            }
            return true;
        }

        protected override string GetFormattedMessage(MessageType messageType, AttributeModifier modifier, Entity user, Entity target)
        {
            var message = GetMessage(modifier.TypeId, messageType);
            var item = modifier.Owner.GetComponent<ItemComponent>(ComponentType.Item);
            if (item.Equipped)
            {
                return "You unequip the " + modifier.Owner.Name;
            }
            else
            {
                return "You equip the " + modifier.Owner.Name;
            }
        }
    }
}
