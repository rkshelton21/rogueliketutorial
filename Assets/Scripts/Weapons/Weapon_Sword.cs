﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Weapons
{
    [Serializable]
    public class Weapon_Sword : Weapon
    {
        public Weapon_Sword()
        {
            _WeaponType = Constants.WeaponType.Sword;
            Description = "It's just a sword, but at least you can stab stuff with it.";

            TypeId = (int)_WeaponType;
            Ammount = +5;
            Modification = AttributeModifier.ModificationType.Additive;
            Target = AttributeModifier.AttributeTarget.DamageBonus;
            ApplyModificationToTarget = base.Apply;
        }

        protected override string GetMessage(int weaponType, MessageType mt)
        {
            switch (weaponType)
            {
                case (int)Constants.WeaponType.Sword:
                    switch (mt)
                    {
                        case MessageType.PlayerModified:
                            return "You equip the sword.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }
    }
}