﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Weapons
{
    [Serializable]
    public class Weapon_Shield : Weapon
    {
        public Weapon_Shield()
        {
            _WeaponType = Constants.WeaponType.Shield;
            Description = "Just your standard round shield, used to help deflect damage.";

            TypeId = (int)_WeaponType;
            Ammount = +2;
            Modification = AttributeModifier.ModificationType.Additive;
            Target = AttributeModifier.AttributeTarget.DefenseBonus;
            ApplyModificationToTarget = base.Apply;
        }

        protected override string GetMessage(int weaponType, MessageType mt)
        {
            switch (weaponType)
            {
                case (int)Constants.WeaponType.Shield:
                    switch (mt)
                    {
                        case MessageType.PlayerModified:
                            return "You equip the shield.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }
    }
}