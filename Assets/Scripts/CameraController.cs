﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var cam = transform.GetComponent<Camera>();
        transform.position = new Vector3(Constants.FullWidth / 2f, Constants.FullHeight / 2f - 0.5f, transform.position.z);

        float screenHeightInUnits = Camera.main.orthographicSize * 2;
        float screenWidthInUnits = screenHeightInUnits * Screen.width / Screen.height; // basically height * screen aspect ratio

        cam.orthographicSize = Constants.FullHeight / 2f;
    }

	// Update is called once per frame
	void Update () {

	}
}
