﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class GameState
    {
        private static bool _UIRendered = false;

        public enum State
        {
            MainMenu,
            Loading,
            PlayerMoving,
            MonstersMoving,
            ViewingInventory,
            ViewingItem,
            Debugging,
            Targeting,
        }

        public static State CurrentState = State.MainMenu;

        public static bool ToggleTargeting(Entity item)
        {
            if (CurrentState != State.ViewingItem && CurrentState != State.Targeting) return false;

            if (CurrentState == State.Targeting)
            {
                UIRender.TargetingRender.CloseTargeting();
                UIRender.ItemDetailsRender.CloseItemDetails(true);
                CurrentState = State.ViewingInventory;
            }
            else
            {
                UIRender.ItemDetailsRender.CloseItemDetails(false);
                UIRender.TargetingRender.OpenTargeting(item);
                CurrentState = State.Targeting;
            }
            return true;
        }

        public static bool ToggleInventory()
        {
            if (CurrentState == State.PlayerMoving)
            {
                // Dissable player movement
                ControllerSystem.Player.Owner.GetComponent<PlayerControllerComponent>(ComponentType.Controller).Enabled = false;
                UIRender.InventoryRender.OpenInventory();
                CurrentState = State.ViewingInventory;
                return true;
            }
            else if (CurrentState == State.ViewingInventory)
            {
                // Re-enable player movement
                ControllerSystem.Player.Owner.GetComponent<PlayerControllerComponent>(ComponentType.Controller).Enabled = true;
                UIRender.InventoryRender.CloseInventory();
                CurrentState = State.PlayerMoving;
                return true;
            }
            else if (CurrentState == State.Targeting)
            {
                UIRender.TargetingRender.CloseTargeting();
                UIRender.ItemDetailsRender.CloseItemDetails(true);
                CurrentState = State.ViewingInventory;
                return true;
            }
            else if (CurrentState == State.ViewingItem)
            {
                //UIRender.InventoryRender.OpenInventory();
                UIRender.ItemDetailsRender.CloseItemDetails(false);
                //UIRender.InventoryOptionsRender.OpenInventoryOptions(selectedItem);
                // Re-enable player movement
                ControllerSystem.Player.Owner.GetComponent<PlayerControllerComponent>(ComponentType.Controller).Enabled = true;
                CurrentState = State.PlayerMoving;
            }
            else
            {
                Debug.LogWarning(String.Format("This state change is not allowed. ({0} to {1})", CurrentState, "Inventory-X"));
            }

            return false;
        }

        public static bool ToggleItemDetails(Entity selectedItem)
        {
            if (CurrentState == State.ViewingInventory)
            {
                //UIRender.InventoryRender.CloseInventory();
                UIRender.ItemDetailsRender.OpenItemDetails(selectedItem);
                //UIRender.InventoryOptionsRender.OpenInventoryOptions(selectedItem);
                CurrentState = State.ViewingItem;
            }
            else if (CurrentState == State.ViewingItem)
            {
                //UIRender.InventoryRender.OpenInventory();
                UIRender.ItemDetailsRender.CloseItemDetails(true);
                //UIRender.InventoryOptionsRender.OpenInventoryOptions(selectedItem);
                CurrentState = State.ViewingInventory;
            }
            else
            {
                Debug.LogWarning(String.Format("This state change is not allowed. ({0} to {1})", CurrentState, "Debugger-X"));
            }

            return false;
        }

        public static bool ToggleDebugger()
        {
            if (CurrentState == State.PlayerMoving)
            {
                // Dissable player movement
                ControllerSystem.Player.Owner.GetComponent<PlayerControllerComponent>(ComponentType.Controller).Enabled = false;
                UIRender.DebugRender.OpenDebugger();
                CurrentState = State.Debugging;
                return true;
            }
            else if (CurrentState == State.Debugging)
            {
                // Re-enable player movement
                ControllerSystem.Player.Owner.GetComponent<PlayerControllerComponent>(ComponentType.Controller).Enabled = true;
                UIRender.DebugRender.CloseDebugger();
                CurrentState = State.PlayerMoving;
                return true;
            }
            else
            {
                Debug.LogWarning(String.Format("This state change is not allowed. ({0} to {1})", CurrentState, "Debugger-X"));
            }

            return false;
        }

        public static bool ToggleMainMenu()
        {
            if (CurrentState == State.MainMenu)
            {
                UIRender.MainMenuRender.DeRegisterTiles();
                CurrentState = State.Loading;
            }
            else
            {
                CurrentState = State.MainMenu;
            }
            return true;
        }

        public static bool ToggleUI(bool forceOn = false)
        {
            if (!_UIRendered || forceOn)
            {
                UIRender.LeftPanelRender.RegisterTiles();
                UIRender.MessageRender.RegisterTiles();
                UIRender.DisplayMapRender.RegisterTiles();
                _UIRendered = true;
            }
            else
            {
                UIRender.LeftPanelRender.DeRegisterTiles();
                UIRender.MessageRender.DeRegisterTiles();
                UIRender.DisplayMapRender.DeRegisterTiles();
                _UIRendered = false;
            }

            return true;
        }

        public static void GameOver()
        {
            ControllerSystem.Player = null;
            MessageSystem.ClearMessages();
            ToggleUI();
            GameState.ToggleMainMenu();
            MapSystem.InitializeEmptyMap();
        }
    }
}
