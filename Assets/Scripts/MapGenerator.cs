﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts;

namespace Assets.Scripts
{
    public static class MapGenerator
    {
        private const int RoomSizeMax = 10;
        private const int RoomSizeMin = 4;
        private const int RoomCountMax = 30;
        public static int MaxDepth = 6;
        private const bool AllowTouching = false;

        private static System.Random rand = new System.Random(1);

        public static MapInfo GenerateMap()
        {
            MapInfo info = new MapInfo();
            for (int l = 0; l < MaxDepth; l++)
            {
                var level = new LevelData();
                level.Tiles = new RLTile[Constants.MapWidth][];
                level.Depth = l;

                for (int x = 0; x < Constants.MapWidth; x++)
                {
                    level.Tiles[x] = new RLTile[Constants.MapHeight];
                    for (int y = 0; y < Constants.MapHeight; y++)
                    {
                        var tile = new RLTile(new DisplayProperties() { Representation = CharacterLibrary.Characters.EnvironmentSettings.NotSet, Color = ColorLibrary.Colors.Null });
                        tile.Location = new Vector2Int(x, y);
                        tile.Depth = l;
                        tile.Walkable = false;
                        tile.BlocksSight = true;
                        tile.Explored = false;
                        tile.InFOV = false;
                        tile.TType = RLTile.TileType.Wall;
                        DisplayFactory.InitializeDisplay(tile);

                        var matchingTile = GameObject.Find(string.Format("Tile: {0}, {1}", x, y));
                        var renderer = matchingTile.GetComponent<TileRenderer>();
                        tile.Renderer = renderer;
                        tile.NeedsUpdate = true;

                        var display = tile.GetDisplay();
                        tile.Renderer.TextMesh.text = display.Representation.ToString();
                        tile.Renderer.TextMesh.color = display.Color;
                        tile.Renderer.Background.color = display.Background;

                        level.Tiles[x][y] = tile;
                    }
                }

                info.Levels.Add(level);
            }

            UIRenderer.ReserveTiles(info);

            for (int l = 0; l < MaxDepth; l++)
            {
                CreateRooms(info.Levels[l]);
            }

            AddStairs(info);

            return info;
        }

        public static MapInfo GenerateEmptyMap()
        {
            MapInfo info = new MapInfo();
            var level = new LevelData();
            level.Tiles = new RLTile[Constants.MapWidth][];
            level.Depth = 0;

            for (int x = 0; x < Constants.MapWidth; x++)
            {
                level.Tiles[x] = new RLTile[Constants.MapHeight];
                for (int y = 0; y < Constants.MapHeight; y++)
                {
                    var tile = new RLTile(new DisplayProperties() { Representation = CharacterLibrary.Characters.EnvironmentSettings.NotSet, Color = ColorLibrary.Colors.Null });
                    var rx = (float)rand.NextDouble();
                    tile.Location = new Vector2Int(x, y);
                    tile.Depth = 0;
                    tile.Walkable = false;
                    tile.BlocksSight = true;
                    tile.Explored = true;
                    tile.InFOV = true;
                    tile.TType = RLTile.TileType.NotSet;
                    DisplayFactory.InitializeDisplay(tile);
                    tile.DefaultDisplay.Color = Color.white;
                    tile.DefaultDisplay.Background = new Color(rx * 0.25f, rx * 0.25f, rx * 0.25f);
                    tile.DefaultDisplay.Representation = (char)MappingUtilities.IndexFor(x, y);
                    level.Tiles[x][y] = tile;
                }
            }

            info.Levels.Add(level);

            UIRenderer.ReserveTiles(info);

            return info;
        }

        private static void InitWalkableTile(RLTile t)
        {
            t.BlocksSight = false;
            t.Walkable = true;
            t.Explored = false;
            t.InFOV = false;
            t.TType = RLTile.TileType.Ground;
            DisplayFactory.InitializeDisplay(t);
        }

        private static void CreateRoom(RLTile[][] tiles, RectInt r)
        {
            for (int x=r.xMin; x<=r.xMax; x++)
            {
                for (int y = r.yMin; y <= r.yMax; y++)
                {
                    InitWalkableTile(tiles[x][y]);
                }
            }
        }

        private static void CreateTunnel_H(RLTile[][] tiles, int xStart, int xEnd, int y)
        {
            for (int x = Math.Min(xStart, xEnd); x <= Math.Max(xStart, xEnd); x++)
            {
                InitWalkableTile(tiles[x][y]);
            }
        }

        private static void CreateTunnel_V(RLTile[][] tiles, int yStart, int yEnd, int x)
        {
            for (int y = Math.Min(yStart, yEnd); y <= Math.Max(yStart, yEnd); y++)
            {
                InitWalkableTile(tiles[x][y]);
            }
        }

        private static bool Intersect(RectInt r1, RectInt r2)
        {
            if(r1.xMin > r2.xMax || r2.xMin > r1.xMax)
                return false;
            if (r1.yMin > r2.yMax || r2.yMin > r1.yMax)
                return false;

            return true;
        }

        private static RectInt[] CreateRooms(LevelData level)
        {
            var rooms = new RectInt[RoomCountMax];
            var numRooms = 0;

            for(int i=0; i<RoomCountMax; i++)
            {
                var w = rand.Next(RoomSizeMin, RoomSizeMax + 1);
                var h = rand.Next(RoomSizeMin, RoomSizeMax + 1);
                var x = rand.Next(0, Constants.MapWidth - w - 1);
                var y = rand.Next(0, Constants.MapHeight - h - 1);

                var newRoom = new RectInt(x, y, w, h);
                var allowed = true;

                // run through the other rooms and see if they intersect with this one
                foreach (var room in rooms)
                {
                    if (Intersect(room, newRoom))
                    {
                        allowed = false;
                        break;
                    }
                }

                // TODO: fix ui reserved tile generation
                // there are better ways to do this like make the map smaller.
                // but for now, don't place rooms on ui tiles.
                for (int tileX = newRoom.xMin; tileX <= newRoom.xMax; tileX++)
                {
                    for (int tileY = newRoom.yMin; tileY <= newRoom.yMax; tileY++)
                    {
                        if (level.Tiles[tileX][tileY].ReservedForUI)
                        {
                            allowed = false;
                            break;
                        }
                    }
                }

                if (allowed)
                {
                    // Shrink the room by 1 to force a border wall
                    if (!AllowTouching)
                    {
                        newRoom = new RectInt(newRoom.position + new Vector2Int(1, 1), newRoom.size - new Vector2Int(2, 2));
                    }

                    // this means there are no intersections, so this room is valid
                    // "paint" it to the map's tiles
                    CreateRoom(level.Tiles, newRoom);

                    if (numRooms == 0 && level.Depth == 0)
                    {
                        // No monsters here
                        level.StairsUp = new Vector2Int((int)newRoom.center.x, (int)newRoom.center.y);
                    }
                    else if (numRooms == 0)
                    {
                        EntitySpawner.PlaceEntities(level, newRoom);
                    }
                    else
                    {
                        // all rooms after the first:
                        // connect it to the previous room with a tunnel

                        // center coordinates of previous room
                        var prevCenter = rooms[numRooms - 1].center;

                        // flip a coin (random number that is either 0 or 1)
                        if (rand.Next(0, 2) == 0)
                        {
                            // first move horizontally, then vertically
                            CreateTunnel_H(level.Tiles, (int)prevCenter.x, (int)newRoom.center.x, (int)prevCenter.y);
                            CreateTunnel_V(level.Tiles, (int)prevCenter.y, (int)newRoom.center.y, (int)newRoom.center.x);
                        }
                        else
                        {
                            // first move vertically, then horizontally
                            CreateTunnel_V(level.Tiles, (int)prevCenter.y, (int)newRoom.center.y, (int)prevCenter.x);
                            CreateTunnel_H(level.Tiles, (int)prevCenter.x, (int)newRoom.center.x, (int)newRoom.center.y);
                        }

                        EntitySpawner.PlaceEntities(level, newRoom);
                    }

                    // finally, append the new room to the list
                    rooms[numRooms] = newRoom;
                    numRooms++;
                }
            }

            return rooms;
        }

        private static void AddStairs(MapInfo map)
        {
            foreach(var level in map.Levels)
            {
                if(level.Depth == 0)
                {
                    var downTile = GetStairTile(map, -1, -1, level.Depth);
                    level.StairsDown = downTile.Location;
                    downTile.DefaultDisplay.Representation = CharacterLibrary.Characters.EnvironmentSettings.Stairs_Down;
                    downTile.DefaultDisplay.Color = ColorLibrary.Colors.Wall_Stairs_Down;
                    downTile.TType = RLTile.TileType.Stairs;
                    downTile.BlocksSight = false;
                }
                else
                {
                    var prevDown = map.Levels[level.Depth - 1].StairsDown;
                    var upTile = GetStairTile(map, prevDown.x, prevDown.y, level.Depth);
                    var downTile = GetStairTile(map, -1, -1, level.Depth);
                    level.StairsUp = upTile.Location;
                    upTile.DefaultDisplay.Representation = CharacterLibrary.Characters.EnvironmentSettings.Stairs_Up;
                    upTile.DefaultDisplay.Color = ColorLibrary.Colors.Wall_Stairs_Up;
                    upTile.TType = RLTile.TileType.Stairs;
                    upTile.BlocksSight = false;

                    if (level.Depth != MaxDepth - 1)
                    {
                        level.StairsDown = downTile.Location;
                        downTile.DefaultDisplay.Representation = CharacterLibrary.Characters.EnvironmentSettings.Stairs_Down;
                        downTile.DefaultDisplay.Color = ColorLibrary.Colors.Wall_Stairs_Down;
                        downTile.TType = RLTile.TileType.Stairs;
                        downTile.BlocksSight = false;
                    }
                }
            }
        }

        private static RLTile GetStairTile(MapInfo map, int x, int y, int d, int maxTiles = 99)
        {
            int maxCounter = 100;
            RLTile retVal = null;
            do
            {
                retVal = ExpandingRadialSearch_OpenTile(map, x, y, d, maxTiles);
                if (retVal == null)
                {
                    x = rand.Next(Constants.MapWidth);
                    y = rand.Next(Constants.MapHeight);
                }
                maxCounter--;
            } while (retVal == null && maxCounter > 0);

            if (maxCounter == 0)
                Debug.LogError("Wow, that shouldn't happen.");

            return retVal;
        }

        public static RLTile ExpandingRadialSearch_OpenTile(MapInfo map, int x, int y, int d, int maxTiles)
        {
            var dist = 0;
            var side = "N";
            var length = 0;
            var counter = 0;
            do
            {
                var start = new Vector2Int(x + dist, y + dist);
                for (int i = 1; i <= length && counter < maxTiles; i++)
                {
                    var offset = new Vector2Int();
                    switch (side)
                    {
                        case "N":
                            offset.x = -i;
                            offset.y = 0;
                            break;
                        case "S":
                            offset.x = -dist * 2 + i;
                            offset.y = -dist * 2;
                            break;
                        case "E":
                            offset.x = 0;
                            offset.y = -dist * 2 + i;
                            break;
                        case "W":
                            offset.x = -dist * 2;
                            offset.y = -i;
                            break;
                    }

                    var tile = map.Tile(start + offset, d);

                    // return if it's walkable and not occupied
                    if(tile != null && tile.Walkable && !tile.Occupied)
                    {
                        //return tile;
                    }
                    // return if it's a wall with a walkable unoccupied neighbor
                    if (tile != null && tile.Walkable == false && Neighbors(map, tile, tile.Depth).Any(q => q.Walkable && !q.Occupied))
                    {
                        return tile;
                    }
                    counter++;
                }

                switch (side)
                {
                    case "N":
                        side = "W";
                        break;
                    case "S":
                        side = "E";
                        break;
                    case "E":
                        side = "N";
                        dist++;
                        length = 2 * dist;
                        break;
                    case "W":
                        side = "S";
                        break;
                }

            } while (dist < 3 && counter < maxTiles);

            return null;
        }

        private static List<RLTile> Neighbors(MapInfo map, RLTile t, int depth)
        {
            var ret = new List<RLTile>();
            var nPos = t.Location + new Vector2Int(0, 1);
            var sPos = t.Location + new Vector2Int(0, -1);
            var ePos = t.Location + new Vector2Int(1, 0);
            var wPos = t.Location + new Vector2Int(-1, 0);

            var n = map.Tile(nPos, depth);
            var s = map.Tile(sPos, depth);
            var e = map.Tile(ePos, depth);
            var w = map.Tile(wPos, depth);

            if (n != null) ret.Add(n);
            if (s != null) ret.Add(s);
            if (e != null) ret.Add(e);
            if (w != null) ret.Add(w);

            return ret;
        }
    }
}
