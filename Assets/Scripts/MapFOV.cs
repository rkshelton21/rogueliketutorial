﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class MapFOV
    {
        public const int Algorithm = 0;
        public const bool LightWalls = true;
        public const int Radius = 10;

        public void UpdateFOVRender()
        {
            foreach(var c in CellsInFov())
            {
                c.InFOV = true;
                c.Explored = true;
            }
        }

        private readonly LevelData _level;
        private readonly HashSet<int> _inFov;

        public MapFOV(LevelData level)
        {
            _level = level;
            _inFov = new HashSet<int>();
        }

        internal MapFOV(LevelData level, HashSet<int> inFov)
        {
            _level = level;
            _inFov = inFov;
        }

        /// <summary>
        /// Create and return a deep copy of an existing FieldOfView class
        /// </summary>
        /// <returns>A deep copy of an existing FieldOfViewClass</returns>
        public MapFOV Clone()
        {
            var inFovCopy = new HashSet<int>();
            foreach (int i in _inFov)
            {
                inFovCopy.Add(i);
            }
            return new MapFOV(_level, inFovCopy);
        }

        /// <summary>
        /// Check if the Cell is in the currently computed field-of-view
        /// Field-of-view must first be calculated by calling ComputeFov and/or AppendFov
        /// </summary>
        /// <remarks>
        /// Field-of-view (FOV) is basically a calculation of what is observable in the Map from a given Cell with a given light radius
        /// </remarks>
        /// <example>
        /// Field-of-view can be used to simulate a character holding a light source and exploring a Map representing a dark cavern
        /// Any Cells within the FOV would be what the character could see from their current location and lighting conditions
        /// </example>
        /// <param name="x">X location of the Cell to check starting with 0 as the farthest left</param>
        /// <param name="y">Y location of the Cell to check, starting with 0 as the top</param>
        /// <returns>True if the Cell is in the currently computed field-of-view, false otherwise</returns>
        public bool IsInFov(int x, int y)
        {
            return _inFov.Contains(MappingUtilities.IndexFor(x, y));
        }

        /// <summary>
        /// Performs a field-of-view calculation with the specified parameters.
        /// Field-of-view (FOV) is basically a calculation of what is observable in the Map from a given Cell with a given light radius.
        /// Any existing field-of-view calculations will be overwritten when calling this method.
        /// </summary>
        /// <param name="xOrigin">X location of the Cell to perform the field-of-view calculation with 0 as the farthest left</param>
        /// <param name="yOrigin">Y location of the Cell to perform the field-of-view calculation with 0 as the top</param>
        /// <param name="radius">The number of Cells in which the field-of-view extends from the origin Cell. Think of this as the intensity of the light source.</param>
        /// <param name="lightWalls">True if walls should be included in the field-of-view when they are within the radius of the light source. False excludes walls even when they are within range.</param>
        /// <returns>List of Cells representing what is observable in the Map based on the specified parameters</returns>
        public ReadOnlyCollection<RLTile> ComputeFov(int xOrigin, int yOrigin, int radius, bool lightWalls, int depth)
        {
            ClearFov();
            return AppendFov(xOrigin, yOrigin, radius, lightWalls, depth);
        }

        /// <summary>
        /// Performs a field-of-view calculation with the specified parameters and appends it any existing field-of-view calculations.
        /// Field-of-view (FOV) is basically a calculation of what is observable in the Map from a given Cell with a given light radius.
        /// </summary>
        /// <example>
        /// When a character is holding a light source in a large area that also has several other sources of light such as torches along the walls
        /// ComputeFov could first be called for the character and then AppendFov could be called for each torch to give us the final combined FOV given all the light sources
        /// </example>
        /// <param name="xOrigin">X location of the Cell to perform the field-of-view calculation with 0 as the farthest left</param>
        /// <param name="yOrigin">Y location of the Cell to perform the field-of-view calculation with 0 as the top</param>
        /// <param name="radius">The number of Cells in which the field-of-view extends from the origin Cell. Think of this as the intensity of the light source.</param>
        /// <param name="lightWalls">True if walls should be included in the field-of-view when they are within the radius of the light source. False excludes walls even when they are within range.</param>
        /// <returns>List of Cells representing what is observable in the Map based on the specified parameters</returns>
        public ReadOnlyCollection<RLTile> AppendFov(int xOrigin, int yOrigin, int radius, bool lightWalls, int depth)
        {
            foreach (RLTile borderCell in _level.GetBorderCellsInSquare(xOrigin, yOrigin, radius))
            {
                foreach (RLTile cell in _level.GetCellsAlongLine(xOrigin, yOrigin, borderCell.Location.x, borderCell.Location.y))
                {
                    if ((Math.Abs(cell.Location.x - xOrigin) + Math.Abs(cell.Location.y - yOrigin)) > radius)
                    {
                        break;
                    }
                    if (!cell.BlocksSight)
                    {
                        _inFov.Add(MappingUtilities.IndexFor(cell));
                    }
                    else
                    {
                        if (lightWalls)
                        {
                            _inFov.Add(MappingUtilities.IndexFor(cell));
                        }
                        break;
                    }
                }
            }

            if (lightWalls)
            {
                // Post processing step created based on the algorithm at this website:
                // https://sites.google.com/site/jicenospam/visibilitydetermination
                foreach (RLTile cell in _level.GetCellsInSquare(xOrigin, yOrigin, radius))
                {
                    if (cell.Location.x > xOrigin)
                    {
                        if (cell.Location.y > yOrigin)
                        {
                            PostProcessFovQuadrant(cell.Location.x, cell.Location.y, Quadrant.SE);
                        }
                        else if (cell.Location.y < yOrigin)
                        {
                            PostProcessFovQuadrant(cell.Location.x, cell.Location.y, Quadrant.NE);
                        }
                    }
                    else if (cell.Location.x < xOrigin)
                    {
                        if (cell.Location.y > yOrigin)
                        {
                            PostProcessFovQuadrant(cell.Location.x, cell.Location.y, Quadrant.SW);
                        }
                        else if (cell.Location.y < yOrigin)
                        {
                            PostProcessFovQuadrant(cell.Location.x, cell.Location.y, Quadrant.NW);
                        }
                    }
                }
            }

            return CellsInFov();
        }

        private ReadOnlyCollection<RLTile> CellsInFov()
        {
            var cells = new List<RLTile>();
            foreach (int index in _inFov)
            {
                var x = index % Constants.MapWidth;
                var y = (int)((index - x) / Constants.MapWidth);
                cells.Add(_level.Tiles[x][y]);
            }
            return new ReadOnlyCollection<RLTile>(cells);
        }

        private void ClearFov()
        {
            foreach (var c in CellsInFov())
            {
                c.InFOV = false;
            }
            _inFov.Clear();
        }

        private void PostProcessFovQuadrant(int x, int y, Quadrant quadrant)
        {
            int x1 = x;
            int y1 = y;
            int x2 = x;
            int y2 = y;
            switch (quadrant)
            {
                case Quadrant.NE:
                    {
                        y1 = y + 1;
                        x2 = x - 1;
                        break;
                    }
                case Quadrant.SE:
                    {
                        y1 = y - 1;
                        x2 = x - 1;
                        break;
                    }
                case Quadrant.SW:
                    {
                        y1 = y - 1;
                        x2 = x + 1;
                        break;
                    }
                case Quadrant.NW:
                    {
                        y1 = y + 1;
                        x2 = x + 1;
                        break;
                    }
            }
            if (!IsInFov(x, y) && _level.Tiles[x][y].BlocksSight)
            {
                if ((!_level.Tiles[x1][y1].BlocksSight && IsInFov(x1, y1)) || (!_level.Tiles[x2][y2].BlocksSight && IsInFov(x2, y2))
                     || (!_level.Tiles[x2][y1].BlocksSight && IsInFov(x2, y1)))
                {
                    _inFov.Add(MappingUtilities.IndexFor(x, y));
                }
            }
        }

        private enum Quadrant
        {
            NE = 1,
            SE = 2,
            SW = 3,
            NW = 4
        }
    }
}
