﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Entity
{
    public Guid ID { get; set; }
    public List<BasicComponent> Components { get; set; }
    public DisplayProperties Display;
    public bool Active { get; set; }
    public bool Occupies { get; set; }
    public int RenderOrder { get; set; }
    public Vector2Int GridPosition { get; set; }
    public int Depth { get; set; }
    public String Name { get; set; }
    public String Description { get; set; }

    public Entity()
    {
        ID = Guid.NewGuid();
        Active = true;
        Occupies = true;
        RenderOrder = 10;
        Name = "";
        Components = new List<BasicComponent>();
    }

    public Entity(char representation) : this()
    {
        Name = representation.ToString();
        this.Display.Representation = representation;
        this.Display.Background = ColorLibrary.Colors.Null;
    }

    public void AddComonent(BasicComponent c)
    {
        if (c != null && Components.Count < 10)
        {
            Components.Add(c);
            c.Owner = this;
            c.OwnerId = this.ID;
        }
    }

    public void DeleteComponent(BasicComponent c)
    {
        c.Owner = null;
        c.OwnerId = Guid.Empty;
        Components.Remove(c);
    }

    public BasicComponent GetComponent(ComponentType c)
    {
        return Components.Find(x => x.CType == c);
    }

    public T GetComponent<T>(ComponentType c) where T : BasicComponent
    {
        return Components.Find(x => x.CType == c) as T;
    }
}
