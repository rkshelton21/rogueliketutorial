﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public static class EntityDistribution
    {
        public static int[] DV(int depth, int value)
        {
            return new int[] { depth, value };
        }

        private static List<DistributionDescription> _itemList;
        private static System.Random _rand = new System.Random(1);

        public struct DistributionDescription
        {
            public DistributionDescription(char type)
            {
                MinLevel = 0;
                MaxLevel = 99;
                CountAtDepth = new int[1][] { new int[1] { 0 } };
                ItemType = type;
            }

            public DistributionDescription(char type, params int[][] depth_val) : this(type)
            {
                CountAtDepth = depth_val.ToArray();
            }

            public int MinLevel;
            public int MaxLevel;
            public int[][] CountAtDepth;
            public char ItemType;

            public int GetCount(int depth)
            {
                return ValueAtDepth(depth, CountAtDepth);
            }
        }

        public static int ValueAtDepth(int depth, params int[][] depth_val)
        {
            int max = 0;

            foreach (var d in depth_val)
            {
                if (depth_val[0][0] > depth)
                    break;
                max = depth_val[0][1];
            }

            return max;
        }

        private static void Init()
        {
            if (_itemList == null)
            {
                _itemList = new List<DistributionDescription>();
                _itemList.Add(new DistributionDescription(CharacterLibrary.Characters.EntitySettings.Potion, DV(0, 70)));
                _itemList.Add(new DistributionDescription(CharacterLibrary.Characters.EntitySettings.Scroll, DV(0, 40)));
                _itemList.Add(new DistributionDescription(CharacterLibrary.Characters.EntitySettings.Weapon, DV(0, 10)));
            }
        }

        public static char GetEntity(int depth, List<DistributionDescription> distroList)
        {
            int sum = distroList.Sum(x => x.GetCount(depth));

            var n = _rand.Next(0, sum);
            foreach (var item in distroList)
            {
                if (n < item.GetCount(depth))
                {
                    return item.ItemType;
                }

                n -= item.GetCount(depth);
            }

            return '@';
        }

        public static char GetItem(int depth)
        {
            Init();
            return GetEntity(depth, _itemList);
        }

        public static int MaxMonstersPerRoom(int depth)
        {
            return ValueAtDepth(depth, DV(0, 2), DV(3, 3), DV(5, 5));
        }

        public static int MaxItemsPerRoom(int depth)
        {
            return ValueAtDepth(depth, DV(0, 1), DV(1, 4));
        }
    }
}
