﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRenderer : MonoBehaviour
{
    public Transform TilePrefab;
    private UIRenderer _UIRenderer;
    public bool RenderUI = true;

    void Awake()
    {
        if (TilePrefab == null)
            Debug.LogError("Tile Prefab not set.");

        InstantiateTilePrefabs();
        _UIRenderer = new UIRenderer();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (GameState.CurrentState == GameState.State.Loading) return;

        for (int x = 0; x < Constants.MapWidth; x++)
        {
            for (int y = 0; y < Constants.MapHeight; y++)
            {
                var tile = MapSystem.CurrentLevelTile(x, y);
                if (tile != null)
                {
                    tile.UpdateDisplay();
                }
            }
        }

        if (RenderUI)
        {
            if (_UIRenderer != null)
                _UIRenderer.RenderUI();
            else
                Debug.LogError("Renderer lost.");
        }
        else
        {
            for (int x = 0; x < Constants.MapWidth; x++)
            {
                for (int y = 0; y < Constants.MapHeight; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(x, y);
                    if (tile != null)
                    {
                        tile.NeedsUpdate = true;
                    }
                }
            }
        }


        if (Input.GetKeyDown(KeyCode.Equals))
        {
            MapSystem.NextLevel();
        }
        if (Input.GetKeyDown(KeyCode.Minus))
        {
            MapSystem.PreviousLevel();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            MapSystem.InitializeMap(true);
            _UIRenderer.ReStart();
            GameState.ToggleUI(true);
            GameState.CurrentState = GameState.State.PlayerMoving;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            DataStorage.Save(null);
        }
    }

    private void InstantiateTilePrefabs()
    {
        for (int x = 0; x < Constants.MapWidth; x++)
        {
            for(int y=0; y < Constants.MapHeight; y++)
            {
                var tile = MapSystem.CurrentLevelTile(x, y);
                var pos = new Vector3(x, y, 0f);
                var newTrans = Instantiate(TilePrefab, pos, Quaternion.identity, this.transform);
                newTrans.name = string.Format("Tile: {0}, {1}", x, y);
                var renderer = newTrans.GetComponent<TileRenderer>();
                renderer.InitTile(tile);
            }
        }
    }
}
