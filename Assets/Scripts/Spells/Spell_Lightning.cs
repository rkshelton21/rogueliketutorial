﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Spells
{
    [Serializable]
    public class Spell_Lightning : Spell
    {
        public Spell_Lightning()
        {
            _ScrollType = Constants.ScrollType.Lightning;
            Description = "This is a scroll of lightning, you have no control over its target, it can strike an enemy within a range of 5";
            TypeId = (int)_ScrollType;
            Ammount = -40;
            Modification = AttributeModifier.ModificationType.Additive;
            Target = AttributeModifier.AttributeTarget.HP;
            RequiresTargeting = false;
            ApplyModificationToTarget = ScanAndStrikeEnemy;
        }

        protected override string GetMessage(int scrollType, MessageType mt)
        {
            switch (scrollType)
            {
                case (int)Constants.ScrollType.Lightning:
                    switch (mt)
                    {
                        case MessageType.PlayerModified:
                            return "You feel a sharp tingle and a strong jolt.";
                        case MessageType.TargetModified:
                            return "The {T} is zapped.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }

        public bool ScanAndStrikeEnemy(AttributeModifier modifier, Entity user, Entity target)
        {
            var targetTile = ExpandingRadialSearch_Occupied(MapSystem.CurrentLevel, user.GridPosition.x, user.GridPosition.y, user.Depth, 50);
            if (targetTile == null)
            {
                MessageSystem.AddMessage("You feel disappointed.", ColorLibrary.Colors.Message_Warning);
                modifier.Used = true;
                return false;
            }
            else
            {
                return base.Apply(modifier, user, targetTile.GetFirstEntity());
            }
        }

        private static RLTile ExpandingRadialSearch_Occupied(LevelData level, int x, int y, int d, int maxTiles)
        {
            var dist = 0;
            var side = "N";
            var length = 0;
            var counter = 0;
            do
            {
                var start = new Vector2Int(x + dist, y + dist);
                for (int i = 1; i <= length && counter < maxTiles; i++)
                {
                    var offset = new Vector2Int();
                    switch (side)
                    {
                        case "N":
                            offset.x = -i;
                            offset.y = 0;
                            break;
                        case "S":
                            offset.x = -dist * 2 + i;
                            offset.y = -dist * 2;
                            break;
                        case "E":
                            offset.x = 0;
                            offset.y = -dist * 2 + i;
                            break;
                        case "W":
                            offset.x = -dist * 2;
                            offset.y = -i;
                            break;
                    }

                    var pos = start + offset;
                    var tile = level.Tiles[pos.x][pos.y];

                    // return if it's walkable and not occupied
                    if (tile != null && tile.InFOV && tile.Walkable && tile.Occupied && tile.Location != start)
                    {
                        return tile;
                    }
                    counter++;
                }

                switch (side)
                {
                    case "N":
                        side = "W";
                        break;
                    case "S":
                        side = "E";
                        break;
                    case "E":
                        side = "N";
                        dist++;
                        length = 2 * dist;
                        break;
                    case "W":
                        side = "S";
                        break;
                }

            } while (dist < 5 && counter < maxTiles);

            return null;
        }

    }
}
