﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Spells
{
    [Serializable]
    public class Spell : ModifierItemBase
    {
        protected Constants.ScrollType _ScrollType;

        protected virtual Entity CreateEntityBase()
        {
            var entity = new Entity(CharacterLibrary.Characters.EntitySettings.Scroll);
            entity.Occupies = false;
            entity.RenderOrder = 20;
            entity.Display.Color = ColorLibrary.Colors.Potion;
            return entity;
        }

        public virtual Entity SpawnScroll(Vector2Int position)
        {
            var newEntity = CreateEntityBase();
            newEntity.GridPosition = position;
            AddComponents(newEntity, _ScrollType);

            newEntity.Name = "Scroll of " + _ScrollType.ToString();
            newEntity.Description = this.Description;
            return newEntity;
        }

        public virtual void AddComponents(Entity entity, Constants.ScrollType scrollType)
        {
            entity.AddComonent(this);
            entity.AddComonent(new ItemComponent(CharacterLibrary.Characters.EntitySettings.Scroll) { });
        }

        protected override MessageType GetRequiredAction(AttributeModifier modifier, Entity user, Entity target)
        {
            if (IsPlayer(user) && IsPlayer(target))
            {
                return MessageType.PlayerModified;
            }
            else
            {
                return MessageType.TargetModified;
            }
        }
    }
}
