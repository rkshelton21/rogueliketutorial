﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Spells
{
    [Serializable]
    public class Spell_Fireball : Spell
    {
        public Spell_Fireball()
        {
            _ScrollType = Constants.ScrollType.Fireball;
            Description = "This spell will launch an exploding fireball at the targeted location dealing a range of damage.";

            TypeId = (int)_ScrollType;
            AffectedRange = 3;
            Ammount = -25;
            Modification = AttributeModifier.ModificationType.Additive;
            Target = AttributeModifier.AttributeTarget.HP;
            RequiresTargeting = true;
            ApplyModificationToTarget = SpreadTheFire;
        }

        public bool SpreadTheFire(AttributeModifier modifier, Entity user, Entity target)
        {
            if (target != null)
            {
                base.Apply(modifier, user, target);
                modifier.Used = false;
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override string GetMessage(int scrollType, MessageType mt)
        {

            switch (scrollType)
            {
                case (int)Constants.ScrollType.Fireball:
                    switch (mt)
                    {
                        case MessageType.PlayerModified:
                            return "You feel a burning sensation. Your flesh is singed.";
                        case MessageType.TargetModified:
                            return "The {T} is singed.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }
    }
}
