﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Spells
{
    [Serializable]
    public class Spell_Confuse : Spell
    {
        public Spell_Confuse()
        {
            _ScrollType = Constants.ScrollType.Confusion;
            Description = "This spell will confuse a targeted enemy for a short period of time.";

            TypeId = (int)_ScrollType;
            AffectedRange = 1;
            Ammount = 6;
            Modification = AttributeModifier.ModificationType.Replace;
            Target = AttributeModifier.AttributeTarget.Component;
            RequiresTargeting = true;
            ApplyModificationToTarget = ConfuseEnemy;
        }

        protected override string GetMessage(int scrollType, MessageType mt)
        {

            switch (scrollType)
            {
                case (int)Constants.ScrollType.Confusion:
                    switch (mt)
                    {
                        case MessageType.PlayerModified:
                            return "You feel confused.";
                        case MessageType.TargetModified:
                            return "The {T} is confused.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }

        public bool ConfuseEnemy(AttributeModifier modifier, Entity user, Entity target)
        {
            var messageType = GetRequiredAction(modifier, user, target);
            var message = GetFormattedMessage(messageType, modifier, user, target);
            MessageSystem.AddMessage(message, ColorLibrary.Colors.Message_SpellText);
            modifier.Used = true;
            return true;
        }
    }
}
