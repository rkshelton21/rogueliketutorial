﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Potions
{
    [Serializable]
    public class Potion : ModifierItemBase
    {
        protected Constants.PotionType _PotionType;

        protected virtual Entity CreateEntityBase()
        {
            var entity = new Entity(CharacterLibrary.Characters.EntitySettings.Potion);
            entity.Occupies = false;
            entity.RenderOrder = 20;
            entity.Display.Color = ColorLibrary.Colors.Potion;
            return entity;
        }

        public virtual Entity SpawnPotion(Vector2Int position)
        {
            var newEntity = CreateEntityBase();
            newEntity.GridPosition = position;
            AddComponents(newEntity, _PotionType);

            newEntity.Name = "Potion of " + _PotionType.ToString();
            return newEntity;
        }

        public virtual void AddComponents(Entity entity, Constants.PotionType scrollType)
        {
            entity.AddComonent(this);
            entity.AddComonent(new ItemComponent(CharacterLibrary.Characters.EntitySettings.Potion) { });
        }
    }
}
