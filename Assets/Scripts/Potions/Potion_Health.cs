﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Potions
{
    [Serializable]
    public class Potion_Health : Potion
    {
        public Potion_Health()
        {
            _PotionType = Constants.PotionType.Health;

            Ammount = 10;
            Modification = AttributeModifier.ModificationType.Additive;
            Target = AttributeModifier.AttributeTarget.HP;
            ApplyModificationToTarget = base.Apply;
        }

        protected override string GetMessage(int potionType, MessageType mt)
        {
            switch (potionType)
            {
                case (int)Constants.PotionType.Health:
                    switch (mt)
                    {
                        case MessageType.PlayerNotNeeded:
                            return "You don't need this.";
                        case MessageType.TargetNotNeeded:
                            return "The {T} is already at full health";
                        case MessageType.PlayerModified:
                            return "You feel better.";
                        case MessageType.TargetModified:
                            return "The {T} is healed.";
                        default:
                            return "You don't know what happened.";
                    }
                default:
                    return "You don't know what happened.";
            }
        }

        protected override MessageType GetRequiredAction(AttributeModifier modifier, Entity user, Entity target)
        {
            var combatant = target.GetComponent<Combatant>(ComponentType.Combatant);
            if (IsPlayer(user) && IsPlayer(target))
            {
                if (combatant.HP == combatant.MaxHP)
                    return MessageType.PlayerNotNeeded;

                return MessageType.PlayerModified;
            }
            else
            {
                if (combatant.HP == combatant.MaxHP)
                    return MessageType.TargetNotNeeded;

                return MessageType.TargetModified;
            }
        }
    }
}
