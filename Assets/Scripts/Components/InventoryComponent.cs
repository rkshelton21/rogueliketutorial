﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class InventoryComponent : BasicComponent
    {
        public List<ItemComponent> Items { get; set; }
        public int MaxItems { get; set; }
        public ItemComponent LeftHand { get; set; }
        public ItemComponent RightHand { get; set; }

        public InventoryComponent()
        {
            CType = ComponentType.Inventory;
            Items = new List<ItemComponent>();
            MaxItems = 10;
        }

        public bool AddItem(ItemComponent item)
        {
            if (Items.Count >= MaxItems)
            {
                MessageSystem.AddMessage("Too many items.", ColorLibrary.Colors.Message_Warning);
                return false;
            }
            else
            {
                MessageSystem.AddMessage(String.Format("You pick up the {0}.", item.Owner.Name), ColorLibrary.Colors.Message_Info);
            }

            Items.Add(item);
            return true;
        }

        public override void Reload(Entity owner)
        {
            base.Reload(owner);

            foreach(var i in Items)
            {
                var match = DataStorage.FindEntity(i.OwnerId);
                if (match != null)
                    i.Owner = match;
                else
                    Debug.LogError("Who owns this?");
            }
        }

        public bool Equip(ItemComponent item)
        {
            if (RightHand == null)
            {
                RightHand = item;
                return true;
            }

            if (LeftHand == null)
            {
                LeftHand = item;
                return true;
            }

            return false;
        }

        public bool UnEquip(ItemComponent item)
        {
            if (LeftHand == item)
            {
                LeftHand = null;
                return true;
            }

            if (RightHand == item)
            {
                RightHand = null;
                return true;
            }

            return false;
        }

        //public void UseItem(int index)
        //{
        //    if (Items[index].Consumable)
        //    {
        //        var modifier = Items[index].Owner.GetComponent<AttributeModifier>(ComponentType.AttributeModifier);
        //        if(modifier != null)
        //        {
        //            modifier.Use(ControllerSystem.Player.Owner, ControllerSystem.Player.Owner);
        //        }
        //        else
        //        {
        //            MessageSystem.AddMessage("I don't know how to use that yet.");
        //        }
        //    }
        //    else
        //    {
        //        MessageSystem.AddMessage("I don't know how to use that yet.");
        //    }
        //}
    }
}
