﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class RenderModifier : BasicComponent
    {
        public RenderModifier()
        {
            CType = ComponentType.RenderModifier;
        }

        public int RenderCounter = -1;
        public DisplayProperties Display;
        public bool ModifyText = false;
        public bool ModifyColor = false;
        public bool ModifyBackground = false;

        public override void Update(RLTile tile)
        {
            if (MarkedForDeletion) return;

            if (ModifyText)
                tile.Renderer.TextMesh.text = Display.Representation.ToString();
            if (ModifyColor)
                tile.Renderer.TextMesh.color = Display.Color;
            if (ModifyBackground)
                tile.Renderer.Background.color = Display.Background;

            if (RenderCounter > 0) RenderCounter--;

            if (RenderCounter == 0)
                MarkedForDeletion = true;

            tile.NeedsUpdate = true;
        }
    }
}
