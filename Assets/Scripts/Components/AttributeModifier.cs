﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class AttributeModifier: BasicComponent
    {
        public delegate bool ApplyModifier(AttributeModifier modifier, Entity user, Entity target);

        public enum ModificationType
        {
            Additive,
            Replace,
            None
        }

        public enum AttributeTarget
        {
            HP,
            Component,
            DamageBonus,
            DefenseBonus,
            None
        }

        public int AffectedRange { get; set; }
        public bool RequiresTargeting { get; set; }
        public bool Used { get; set; }
        public float Ammount { get; set; }
        public ModificationType Modification { get; set; }
        public AttributeTarget Target { get; set; }

        [XmlIgnore]
        public ApplyModifier ApplyModificationToTarget { get; set; }

        public AttributeModifier()
        {
            CType = ComponentType.AttributeModifier;
            Used = false;
            RequiresTargeting = false;
            AffectedRange = 1;
        }

        public int GetVisibleTargetRange()
        {
            return 1;
        }

        public List<RLTile> GetAffectedTiles()
        {
            var results = new List<RLTile>();
            var center = MouseSystem.GetHighlightedTile();
            results.Add(center);
            switch (AffectedRange)
            {
                case 1:
                    break;
                case 2:
                    results.AddRange(MappingUtilities.Neighbors(center));
                    break;
                case 3:
                    results.AddRange(MappingUtilities.GetCellsInRange(center.Location, 2));
                    break;
                default:
                    Debug.LogError("That effect range is not supported");
                    break;
            }

            return results;
        }

        public bool UseModifier()
        {
            var targetedTiles = GetAffectedTiles();
            var used = false;
            foreach (var target in targetedTiles)
            {
                var usedNow = ApplyModificationToTarget(this, ControllerSystem.Player.Owner, target.GetFirstEntity());
                used = used || usedNow;
                target.NeedsUpdate = true;
            }

            //Consume
            if (used) Used = true;
            return used;
        }

        public void ApplyToEntity(Entity user, Entity target)
        {
            var tCombatant = target.GetComponent<Combatant>(ComponentType.Combatant);
            switch (this.Target)
            {
                case AttributeTarget.HP:
                    tCombatant.HP += this.Ammount;
                    tCombatant.HP = Math.Max(0, tCombatant.HP);
                    tCombatant.HP = Math.Min(tCombatant.HP, tCombatant.MaxHP);
                    break;
                case AttributeTarget.DamageBonus:
                    tCombatant.DamageBonus += this.Ammount;
                    break;
                case AttributeTarget.DefenseBonus:
                    tCombatant.DefenseBonus += this.Ammount;
                    break;
                default:
                    Debug.Log("Beats me.");
                    break;
            }

            if (!IsPlayer(target))
            {
                if (tCombatant.HP == 0)
                {
                    target.Active = false;
                    target.RenderOrder = 100;
                    target.Display.Representation = CharacterLibrary.Characters.EntitySettings.DeadBody;
                    target.Display.Color = ColorLibrary.Colors.DeadBody;
                    MapSystem.Tile(target.GridPosition.x, target.GridPosition.y, target.Depth).NeedsUpdate = true;

                    MessageSystem.AddMessage(string.Format("The {0} is slain.", target.Name), ColorLibrary.Colors.Message_Warning);
                    if (IsPlayer(user))
                    {
                        var uCombatant = user.GetComponent<Combatant>(ComponentType.Combatant);
                        XPUtilities.AddXP(uCombatant, tCombatant.XP);
                    }
                }
            }
        }

        protected static bool IsPlayer(Entity entity)
        {
            return entity.ID == ControllerSystem.Player.Owner.ID;
        }
    }
}
