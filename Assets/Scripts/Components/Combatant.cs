﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class Combatant: BasicComponent
    {
        public Combatant()
        {
            CType = ComponentType.Combatant;
            Level = 1;
        }

        public int Level { get; set; }
        public int XP { get; set; }

        public float MaxHP { get; set; }
        public float HP { get; set; }

        public float Damage { get; set; }
        public float Defense { get; set; }
        public float DamageBonus { get; set; }
        public float DefenseBonus { get; set; }
    }
}
