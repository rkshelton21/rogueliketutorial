﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Components
{
    public enum ComponentType
    {
        None,
        Controller,
        Combatant,
        Item,
        Inventory,
        AttributeModifier,
        RenderModifier
    }
}
