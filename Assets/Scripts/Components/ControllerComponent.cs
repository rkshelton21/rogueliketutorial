﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts;
using RogueSharp;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class ControllerComponent : BasicComponent
    {
        public bool Enabled { get; set; }
        public AIBehavior Behaviour { get; set; }
        private System.Random _rand;
        protected Path _path;

        public ControllerComponent()
        {
            CType = ComponentType.Controller;
            Enabled = true;
            Behaviour = AIBehavior.NotSet;
            _rand = new System.Random(1);
        }

        public MovementRules.MovementStatus Move(Vector2Int input)
        {
            if (!Enabled) return MovementRules.MovementStatus.NotSet;

            if(input == new Vector2Int())
                input = GetInput();

            if(Owner == null) return MovementRules.MovementStatus.Idle;

            var destination = Owner.GridPosition + input;
            if (input.magnitude == 0)
            {
                //idle
                return MovementRules.MovementStatus.Idle;
            }

            var moveResult = MovementRules.AllowMove(destination, Owner.Depth);

            switch(moveResult)
            {
                case MovementRules.MovementStatus.Blocked:
                    return moveResult;
                case MovementRules.MovementStatus.Valid:
                    Owner.GridPosition = destination;
                    MapSystem.AddToMap(Owner);
                    return moveResult;
                case MovementRules.MovementStatus.Occupied:
                    var destinationTile = MapSystem.Tile(destination.x, destination.y, Owner.Depth);
                    var enemy = destinationTile.GetFirstEntity();
                    //MessageSystem.AddMessage(String.Format("You kick the {0} in the shins, much to its annoyance!", enemy.Name));
                    DamageSystem.PerformAttack(Owner, enemy, destinationTile.InFOV);
                    return moveResult;
            }

            return moveResult;
        }

        protected virtual Vector2Int GetInput()
        {
            return new Vector2Int();
        }

        public Vector2Int MoveToward(Vector2Int target)
        {
            var dx = target.x - Owner.GridPosition.x;
            var dy = target.y - Owner.GridPosition.y;

            var x = dx == 0 ? 0 : dx / Math.Abs(dx);
            var y = dy == 0 ? 0 : dy / Math.Abs(dy);

            return GetBestOption(x, y);
        }

        public float DistanceTo(Vector2Int target)
        {
            var dx = target.x - Owner.GridPosition.x;
            var dy = target.y - Owner.GridPosition.y;
            var distance = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
            return distance;
        }

        private Vector2Int GetBestOption(int dx, int dy)
        {
            var currentX = Owner.GridPosition.x;
            var currentY = Owner.GridPosition.y;

            var o1Valid = MapSystem.Tile(currentX + dx, currentY + dy, Owner.Depth);
            // take diagonal if it's available
            //if (o1Valid.Walkable)
            //    return new Vector2Int(dx, dy);

            var o2Valid = MapSystem.Tile(currentX, currentY + dy, Owner.Depth);
            var o3Valid = MapSystem.Tile(currentX + dx, currentY, Owner.Depth);

            var options = new int[3];
            var optionCount = 0;

            if (o1Valid.Walkable && dx != 0)
            {
                options[optionCount++] = 1;
            }
            if (o2Valid.Walkable && dy != 0)
            {
                options[optionCount++] = 2;
            }
            if (o3Valid.Walkable && dx != 0 && dy != 0)
            {
                options[optionCount++] = 3;
            }

            var choice = _rand.Next(0, optionCount);
            switch(options[choice])
            {
                case 1:
                    return new Vector2Int(dx, 0);
                case 2:
                    return new Vector2Int(0, dy);
                case 3:
                    return new Vector2Int(dx, dy);
                default:
                    break;
            }

            return new Vector2Int();
        }

        public Path GetPath()
        {
            return _path;
        }
    }
}
