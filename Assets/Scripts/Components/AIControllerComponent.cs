﻿using Assets.Scripts.Components;
using RogueSharp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using System;

[Serializable]
public class AIControllerComponent : ControllerComponent
{
    private RLTile _target;
    private AIBehavior _behavior;

    protected override Vector2Int GetInput()
    {
        if (!Enabled) return new Vector2Int();
        if (!Owner.Active) return new Vector2Int();

        _behavior = BehaviorSystem.GetBehavior(Owner, _behavior);

        switch (_behavior)
        {
            case AIBehavior.Wandering:
                return Wander();
            case AIBehavior.Hunting:
                return Hunt();
            default:
                return new Vector2Int();
        }
    }

    private void GetNewWanderingPath()
    {
        var pos = Owner.GridPosition;
        var currentTile = MapSystem.Tile(pos.x, pos.y, Owner.Depth);
        var targetTile = MapSystem.RandomWalkableTile(Owner.Depth);
        if (currentTile.Location == targetTile.Location)
        {
            _path = null;
        }
        else
        {
            _path = PathingSystem.GetPath(currentTile, targetTile, targetTile.Depth);
        }
    }

    private Vector2Int FollowPath()
    {
        if (_path == null) return new Vector2Int();

        if (_target == null)
        {
            _target = _path.StepForward();
        }
        else
        {
            var pos = Owner.GridPosition;
            var t = _target.Location;
            if (pos == t)
            {
                if (_path.End.Location == Owner.GridPosition)
                    _path = null;
                else
                    _target = _path.StepForward();
            }
        }

        if (_path != null && _path.End.Location == Owner.GridPosition)
            _path = null;

        return MoveToward(_target.Location);
    }

    private Vector2Int Wander()
    {
        if (_path != null)
        {
            return FollowPath();
        }
        else
        {
            GetNewWanderingPath();
        }

        return new Vector2Int();
    }

    private Vector2Int Hunt()
    {
        var cPos = Owner.GridPosition;
        var pPos = ControllerSystem.Player.Owner.GridPosition;
        var currentTile = MapSystem.Tile(cPos.x, cPos.y, Owner.Depth);
        var targetTile = MapSystem.Tile(pPos.x, pPos.y, Owner.Depth);
        if (currentTile.Location == targetTile.Location)
        {
            _path = null;
        }
        else
        {
            _path = PathingSystem.GetPath(currentTile, targetTile, targetTile.Depth);
        }

        return FollowPath();
    }
}
