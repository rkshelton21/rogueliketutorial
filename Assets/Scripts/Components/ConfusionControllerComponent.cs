﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class ConfusionControllerComponent : AIControllerComponent
    {
        private System.Random _rand = new System.Random();
        public AIControllerComponent OriginalComponent;

        public int Counter { get; set; }
        public ConfusionControllerComponent() { }

        public ConfusionControllerComponent(Entity entity, int counter)
        {
            Owner = entity;
            OwnerId = entity.ID;
            OriginalComponent = entity.GetComponent<AIControllerComponent>(ComponentType.Controller);
            OriginalComponent.Enabled = false;
            //entity.DeleteComponent(OriginalComponent);
            entity.AddComonent(this);
            Counter = counter;
            ControllerSystem.Register(this);
        }

        protected override Vector2Int GetInput()
        {
            if (!Enabled) return new Vector2Int();
            if (!Owner.Active) return new Vector2Int();

            Counter--;

            if (Counter < 0)
            {
                OriginalComponent.Enabled = true;
                Enabled = false;
                Owner.DeleteComponent(this);
                //Owner.AddComonent(OriginalComponent);
                //OriginalComponent.Owner = Owner;
                //Owner = null;
                return new Vector2Int();
            }

            return new Vector2Int(_rand.Next(0, 3) - 1, _rand.Next(0, 3) - 1);
        }
    }
}
