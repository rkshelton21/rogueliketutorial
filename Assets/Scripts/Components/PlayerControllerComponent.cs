﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerControllerComponent : ControllerComponent
{
    private Vector2 touchOrigin = -Vector2.one;
    private float SwipeThreshold = 1.0f;

    protected override Vector2Int GetInput()
    {
        if (!Enabled || !Owner.Active) return new Vector2Int();

        var input = new Vector2Int();
        var w = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Keypad8);
        var a = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Keypad4);
        var x = Input.GetKey(KeyCode.X) || Input.GetKey(KeyCode.Keypad2);
        var d = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.Keypad6);
        var q = Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.Keypad7);
        var e = Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Keypad9);
        var z = Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.Keypad1);
        var c = Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.Keypad3);
        var s = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Keypad5);

        Touch myTouch = new Touch();
        var swipeToMove = true;
        var touchCount = Input.touchCount;

        if(touchCount > 0)
        {
            myTouch = Input.touches[0];
        }
        else if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                myTouch.phase = TouchPhase.Began;
                myTouch.position = Input.mousePosition;
                touchCount = 1;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            myTouch.phase = TouchPhase.Ended;
            myTouch.position = Input.mousePosition;
            touchCount = 1;
        }

        if (swipeToMove && touchCount > 0)
        {
            var horizontal = 0;
            var vertical = 0;
            if (myTouch.phase == TouchPhase.Began)
            {
                touchOrigin = GetPosition(myTouch.position);
            }
            else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
            {
                Vector2 touchEnd = GetPosition(myTouch.position);
                float touchX = touchEnd.x - touchOrigin.x;
                float touchY = touchEnd.y - touchOrigin.y;

                if(Mathf.Abs(touchX) < SwipeThreshold && Mathf.Abs(touchY) < SwipeThreshold)
                    return new Vector2Int();

                var xOnMap = touchOrigin.x >= Constants.MapRender.xMin && touchOrigin.x <= Constants.MapRender.xMax;
                var yOnMap = touchOrigin.y >= Constants.MapRender.yMin && touchOrigin.y <= Constants.MapRender.yMax;
                if(!xOnMap || !yOnMap)
                    return new Vector2Int();

                touchOrigin.x = -1;

                if (Mathf.Abs(touchX) > Mathf.Abs(touchY))
                {
                    horizontal = touchX > 0 ? 1 : -1;
                    if(horizontal > 0)
                    {
                        d = true;
                    } else if(horizontal < 0)
                    {
                        a = true;
                    }
                }
                else
                {
                    vertical = touchY > 0 ? 1 : -1;
                    if (vertical > 0)
                    {
                        w = true;
                    }
                    else if (vertical < 0)
                    {
                        x = true;
                    }
                }
            }
        }

        if (w || x || q || e || z || c)
        {
            input.y = w || q || e ? 1 : -1;
            return input;
        }

        if (a || d || q || e || z || c)
        {
            input.x = d || e || c ? 1 : -1;
            return input;
        }

        //if(s)
        //{
        //    Test();
        //}

        return input;
    }

    private void Test()
    {
        var targetPos = new Vector2Int(74, 26);
        var pos = Owner.GridPosition;
        Debug.Log(string.Format("{0} location: {1}, {2}", CharacterLibrary.Characters.EntitySettings.Player, pos.x, pos.y));

        //kill all except 74,26
        for (int x = 0; x < Constants.MapWidth; x++)
        {
            for (int y = 0; y < Constants.MapHeight; y++)
            {
                var tile = MapSystem.CurrentLevelTile(x, y);
                if (!(x == targetPos.x && y == targetPos.y) && !(x == pos.x && y == pos.y))
                {
                    Entity e = null;
                    do
                    {
                        e = tile.GetFirstEntity();
                        if (e != null)
                        {
                            e.Active = false;
                            tile.Remove(e);
                        }
                    } while (e != null);
                }
            }
        }
    }

    private Vector2Int GetPosition(Vector3 mousePosition)
    {
        //if(GameState.CurrentState == GameState.State.Targeting)
        //return new Vector2Int(30, 10);
        Camera c = Camera.main;
        var p = c.ScreenToWorldPoint(mousePosition);
        return new Vector2Int((int)(p.x + 0.5f), (int)(p.y + 0.5f));
    }

}
