﻿using Assets.Scripts.Components;
using Assets.Scripts.Potions;
using Assets.Scripts.Spells;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

[Serializable]
[System.Xml.Serialization.XmlInclude(typeof(AIControllerComponent))]
[System.Xml.Serialization.XmlInclude(typeof(AttributeModifier))]
[System.Xml.Serialization.XmlInclude(typeof(Combatant))]
[System.Xml.Serialization.XmlInclude(typeof(PlayerControllerComponent))]
[System.Xml.Serialization.XmlInclude(typeof(ItemComponent))]
[System.Xml.Serialization.XmlInclude(typeof(InventoryComponent))]
[System.Xml.Serialization.XmlInclude(typeof(Spell))]
[System.Xml.Serialization.XmlInclude(typeof(Potion))]
[System.Xml.Serialization.XmlInclude(typeof(Potion_Health))]
[System.Xml.Serialization.XmlInclude(typeof(Spell_Fireball))]
[System.Xml.Serialization.XmlInclude(typeof(Spell_Confuse))]
[System.Xml.Serialization.XmlInclude(typeof(Spell_Lightning))]
public abstract class BasicComponent
{
    public ComponentType CType { get; set; }
    public int TypeId { get; set; }

    [XmlIgnore]
    public Entity Owner;
    public Guid OwnerId;
    public bool MarkedForDeletion { get; set; }
    public String Description { get; set; }

    public BasicComponent()
    {
        CType = ComponentType.None;
        MarkedForDeletion = false;
    }

    public BasicComponent(Entity owner)
    {
        CType = ComponentType.None;
        Owner = owner;
        OwnerId = owner.ID;
        MarkedForDeletion = false;
    }

    public virtual void Update()
    {
        throw new NotImplementedException();
    }

    public virtual void Update(RLTile tile)
    {
        throw new NotImplementedException();
    }

    public virtual void Reload(Entity owner)
    {
        this.Owner = owner;
    }
}
