﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Components
{
    [Serializable]
    public class ItemComponent: BasicComponent
    {
        public bool Equipable { get; set; }
        public bool Consumable { get; set; }
        public bool Equipped { get; set; }

        public ItemComponent()
        {
            Equipable = false;
            Consumable = false;
        }

        public ItemComponent(char type)
        {
            CType = ComponentType.Item;

            if (type == CharacterLibrary.Characters.EntitySettings.Potion || type == CharacterLibrary.Characters.EntitySettings.Scroll)
                Consumable = true;
            if (type == CharacterLibrary.Characters.EntitySettings.Weapon)
                Equipable = true;
        }

        public void CollectItem(Entity consumer)
        {
            var x = Owner.GridPosition.x;
            var y = Owner.GridPosition.y;
            MapSystem.Tile(x, y, Owner.Depth).Remove(this.Owner);

            consumer.GetComponent<InventoryComponent>(ComponentType.Inventory).AddItem(this);
        }
    }
}
