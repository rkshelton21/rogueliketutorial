﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TestRender : MonoBehaviour {
    public int CharacterCode = 0;
    public Vector4 ForeGround;
    public Vector4 BackGround;
    TextMesh _textMesh;
    SpriteRenderer _background;
    private bool _initialized = false;

    // Use this for initialization
	void Init () {
        if (_initialized) return;
        _initialized = true;
        _textMesh = GetComponent<TextMesh>();
        _background = transform.Find("Background").GetComponent<SpriteRenderer>();

        GetAllFromObject();
    }
	
    void Update()
    {
        Init();

        SetFromInspector();
        GetFromObject();
    }

    void GetAllFromObject()
    {
        if (_textMesh.text.Length > 0)
            CharacterCode = (int)_textMesh.text[0];
        else
            CharacterCode = 0;

        GetFromObject();
    }

    void GetFromObject()
    {
        ForeGround = new Vector4(_textMesh.color.r, _textMesh.color.g, _textMesh.color.b, _textMesh.color.a);
        BackGround = new Vector4(_background.color.r, _background.color.g, _background.color.b, _background.color.a);
    }

    void SetFromInspector()
    {
        _textMesh.text = ((char)CharacterCode).ToString();        
    }
}