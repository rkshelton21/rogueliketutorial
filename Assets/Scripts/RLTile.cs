﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

[Serializable]
public class RLTile
{
    public delegate void RenderTile(RLTile tile);

    public enum TileType { Ground, Wall, Stairs, NotSet };
    public Vector2Int Location { get { return _Location; } set { NeedsUpdate = true; _Location = value; } }
    public bool BlocksSight { get { return _BlocksSight; } set { NeedsUpdate = true; _BlocksSight = value; } }
    public bool Walkable { get { return _Walkable; } set { NeedsUpdate = true; _Walkable = value; } }
    public bool Explored { get { return _Explored; } set { NeedsUpdate = true; _Explored = value; } }
    public bool Highlighted { get { return _Highlighted; } set { NeedsUpdate = true; _Highlighted = value; } }
    public bool Hovered { get { return _Hovered; } set { NeedsUpdate = true; _Hovered = value; } }
    public bool InFOV { get { return _InFOV; } set { NeedsUpdate = true; _InFOV = value; } }
    public bool NeedsUpdate { get; set; }
    public bool ReservedForUI { get; set; }

    public TileType TType;
    [XmlIgnore]
    public String TestData;

    [XmlIgnore]
    public TileRenderer Renderer;
    public DisplayProperties DefaultDisplay;
    public int Depth;

    [XmlIgnore]
    public RenderTile RenderFunction;
    [XmlIgnore]
    public RenderTile DefaultRenderFunction;
    private List<Entity> _characterReferences = new List<Entity>();
    private List<BasicComponent> _components { get; set; }
    private Vector2Int _Location;
    private bool _BlocksSight { get; set; }
    private bool _Walkable { get; set; }
    private bool _Explored { get; set; }
    private bool _Highlighted { get; set; }
    private bool _Hovered { get; set; }
    private bool _InFOV { get; set; }

    public RLTile()
    {
        NeedsUpdate = false;
        ReservedForUI = false;
        _BlocksSight = false;
        _Walkable = true;
        _Explored = false;
        _Highlighted = false;
        _Hovered = false;
        _InFOV = false;
    }

    public bool HasItem
    {
        get
        {
            return _characterReferences.Any(c => c.GetComponent<ItemComponent>(ComponentType.Item) != null);
        }
    }
    public bool Occupied
    {
        get
        {
            return _characterReferences.Any(c => c.Occupies && c.GridPosition == this.Location && c.Active);
        }
    }

    public int X { get { return Location.x; } }
    public int Y { get { return Location.y; } }

    public RLTile(DisplayProperties d)
    {
        DefaultDisplay = d;
    }

    public void Add(Entity c)
    {
        if (c == null) return;
        NeedsUpdate = true;

        if (!_characterReferences.Any(x => x.ID == c.ID))
            _characterReferences.Add(c);

        if (ControllerSystem.Player != null && ControllerSystem.Player.Owner != null && c.ID == ControllerSystem.Player.Owner.ID)
        {
            if(HasItem)
            {
                var inv = c.GetComponent<InventoryComponent>(ComponentType.Inventory);
                var item = _characterReferences.First(x => x.GetComponent<ItemComponent>(ComponentType.Item) != null);
                var added = inv.AddItem(item.GetComponent<ItemComponent>(ComponentType.Item));
                if(added)
                    Remove(item);
            }
        }
    }

    public void Add(BasicComponent c)
    {
        if (_components == null) _components = new List<BasicComponent>();

        _components.Add(c);
    }

    public void Remove(BasicComponent c)
    {
        if (_components == null) return;

        _components.Remove(c);
    }

    public void Remove(Entity c)
    {
        NeedsUpdate = true;

        if (_characterReferences.Any(x => x.ID == c.ID))
            _characterReferences.Remove(c);
    }

    public void UpdateEntities()
    {
        PurgeEntities();
    }

    public DisplayProperties GetDisplay(bool forRender = true)
    {
        var ignoreHighlights = false;
        if (forRender)
            NeedsUpdate = false;
        else
            ignoreHighlights = true;

        if ((_characterReferences.Count == 0 || !InFOV) && !MapSystem.DebugRendering())
        {
            return DisplayFactory.GetDisplay(this, DefaultDisplay, ignoreHighlights: ignoreHighlights);
        }

        if (_characterReferences.Count > 0)
        {
            return DisplayFactory.GetDisplay(this, _characterReferences.OrderBy(x => x.RenderOrder).First().Display, true, ignoreHighlights: ignoreHighlights);
        }
        else
        {
            return DisplayFactory.GetDisplay(this, DefaultDisplay, ignoreHighlights: ignoreHighlights);
        }
    }

    public void UpdateDisplay(bool forceUpdate = false)
    {
        if (TestData == "Q")
            TestData = "Q";

        //if (Renderer.TextMesh.text == "Q")
        //    forceUpdate = true;

        //if ((NeedsUpdate || forceUpdate) && Renderer != null)
        //{
        //    var display = GetDisplay();
        //    Renderer.TextMesh.text = display.Representation.ToString();
        //    Renderer.TextMesh.color = display.Color;
        //    Renderer.Background.color = display.Background;
        //}

        if(RenderFunction == null)
        {
            if (DefaultRenderFunction != null)
            {
                DefaultRenderFunction(this);
            }
            else
            {
                //Renderer.TextMesh.text = "+";
                //Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                //Renderer.Background.color = ColorLibrary.Colors.UIBase;
                Renderer.TextMesh.color = Color.black;
                Renderer.Background.color = Color.black;
            }
        }


        //if (_components != null)
        //{
        //    foreach (var c in _components)
        //    {
        //        c.Update(this);
        //    }

        //    _components.RemoveAll(x => x.MarkedForDeletion);
        //}

        if(RenderFunction != null)
            RenderFunction(this);
    }

    public void ModifyRenderer(RLTile targetTile)
    {
        if (_components != null)
        {
            foreach (var c in _components)
            {
                c.Update(targetTile);
            }

            _components.RemoveAll(x => x.MarkedForDeletion);
        }
    }

    public Entity GetFirstEntity()
    {
        PurgeEntities();

        if (_characterReferences.Count == 0)
            return null;

        return _characterReferences.OrderBy(x => x.RenderOrder).First();
    }

    public List<Entity> GetAllEntities()
    {
        PurgeEntities();

        return _characterReferences;
    }

    private void PurgeEntities()
    {
        if (_characterReferences.Count > 0)
        {
            int removed = _characterReferences.RemoveAll(c => c == null || c.GridPosition != this.Location);
            if (removed > 0)
            {
                NeedsUpdate = true;
            }
        }
    }
}
