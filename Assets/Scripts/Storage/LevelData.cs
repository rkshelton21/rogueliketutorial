﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class LevelData
    {
        public LevelData()
        {
            //Entities = new List<Entity>();
        }

        public int Depth { get; set; }
        public RLTile[][] Tiles { get; set; }
        public Vector2Int StairsUp { get; set; }
        public Vector2Int StairsDown { get; set; }
        //public List<Entity> Entities { get; set; }
    }
}
