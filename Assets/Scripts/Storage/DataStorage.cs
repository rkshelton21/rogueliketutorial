﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts
{
    public static class DataStorage
    {
        public static GameData savedData = null;

        public static void writeStringToFile(string str, string filename)
        {
#if !WEB_BUILD
            string path = pathForDocumentsFile(filename);
            FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);

            StreamWriter sw = new StreamWriter(file);
            sw.Write(str);

            sw.Close();
            file.Close();
#endif
        }


        public static string readStringFromFile(string filename)//, int lineIndex )
        {
#if !WEB_BUILD
            string path = pathForDocumentsFile(filename);

            if (File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);

                string str = null;
                str = sr.ReadToEnd();

                sr.Close();
                file.Close();

                return str;
            }

            else
            {
                return null;
            }
#else
return null;
#endif
        }


        public static string pathForDocumentsFile(string filename)
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(Path.Combine(path, "Documents"), filename);
            }

            else if (Application.platform == RuntimePlatform.Android)
            {
                string path = Application.persistentDataPath;
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(path, filename);
            }

            else
            {
                string path = Application.dataPath;
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(path, filename);
            }
        }

        public static GameData GetCurrentGameData()
        {
            GameData gd = new GameData();
            gd.CurrentState = GameState.CurrentState;
            gd.Player = ControllerSystem.Player.Owner;
            gd.Levels = new List<LevelData>();
            gd.LevelEntities = new List<List<Entity>>();
            gd.CurrentLevel = MapSystem.CurrentDepth;

            foreach (var l in MapSystem.Levels)
            {
                var currentLevel = new LevelData();
                var currentEntities = new List<Entity>();
                for (int x = 0; x < Constants.MapWidth; x++)
                {
                    for (int y = 0; y < Constants.MapHeight; y++)
                    {
                        var t = MapSystem.Tile(x, y, l.Depth);
                        var eList = t.GetAllEntities();
                        currentEntities.AddRange(eList);
                    }
                }
                currentEntities = currentEntities.Distinct().ToList();
                currentLevel.Tiles = l.Tiles;
                currentLevel.StairsDown = l.StairsDown;
                currentLevel.StairsUp = l.StairsUp;

                gd.Levels.Add(currentLevel);
                gd.LevelEntities.Add(currentEntities);

                // add players items
                var inv = gd.Player.GetComponent<InventoryComponent>(ComponentType.Inventory);
                foreach(var item in inv.Items)
                {
                    gd.LevelEntities[0].Add(item.Owner);
                }
            }
            gd.MessageQueue = MessageSystem.GetMessages();

            return gd;
        }

        //it's static so we can call it from anywhere
        public static void Save(GameData data)
        {
            if (data == null)
                data = GetCurrentGameData();

            DataStorage.savedData = data;

            try
            {
                //BinaryFormatter bf = new BinaryFormatter();
                ////Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
                //FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
                //bf.Serialize(file, SaveLoad.savedData);
                //file.Close();

                var s = SerializeObject<GameData>(DataStorage.savedData);
                writeStringToFile(s, "savedGames.gd");
            }
            catch (Exception exc)
            {
                Debug.LogError(exc.Message);
            }

            SaveDisplaySettings();
        }

        public static void SaveDisplaySettings()
        {
            try
            {
                var s = SerializeObject<CharacterLibrary>(CharacterLibrary.Characters);
                writeStringToFile(s, "characterLibrary.gd");
            }
            catch (Exception exc)
            {
                Debug.LogError(exc.Message);
            }

            try
            {
                var s = SerializeObject<ColorLibrary>(ColorLibrary.Colors);
                writeStringToFile(s, "colorLibrary.gd");
            }
            catch (Exception exc)
            {
                Debug.LogError(exc.Message);
            }
        }

        public static ColorLibrary LoadColorLibrary()
        {
            try
            {
                var s = readStringFromFile("colorLibrary.gd");
                return DeserializeObject<ColorLibrary>(s);
            }
            catch (Exception exc)
            {
                Debug.LogError("Could not load: " + exc.Message);
            }
            return null;
        }

        public static CharacterLibrary LoadCharacterLibrary()
        {
            try
            {
                var s = readStringFromFile("characterLibrary.gd");
                return DeserializeObject<CharacterLibrary>(s);
            }
            catch (Exception exc)
            {
                Debug.LogError("Could not load: " + exc.Message);
            }
            return null;
        }

        public static void Load(bool reload = false)
        {
            if (savedData == null || reload)
            {
                try
                {
                    //savedData = new GameData();
                    //savedData.HighScore = 0;
                    //savedData.MatchRequirement = 3;
                    //savedData.ColorPalette = 0;
                    //savedData.BlockPalette = 0;
                    //savedData.CurrentLevel = 1;

                    //if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
                    //{
                    //    BinaryFormatter bf = new BinaryFormatter();
                    //    FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
                    //    SaveLoad.savedData = (GameData)bf.Deserialize(file);
                    //    file.Close();
                    //}

                    var s = readStringFromFile("savedGames.gd");
                    DataStorage.savedData = DeserializeObject<GameData>(s);

                    InitTiles();
                    UpdateEntities();
                    //MapSystem.RemapRenderersToNewTiles();
                }
                catch (Exception exc)
                {
                    Debug.LogError("Could not load: " + exc.Message);
                    DataStorage.savedData = null;
                }
            }
        }

        public static Entity FindEntity(Guid id)
        {
            foreach (var level in DataStorage.savedData.LevelEntities)
            {
                var m = level.Find(x => x.ID == id);
                if (m != null)
                    return m;
            }

            return null;
        }

        private static void UpdateEntities()
        {
            var i = 0;
            foreach (var level in DataStorage.savedData.LevelEntities)
            {
                foreach (var e in level)
                {
                    if (e.ID == DataStorage.savedData.Player.ID)
                    {
                        DataStorage.savedData.Player = e;
                    }

                    foreach (var c in e.Components)
                    {
                        c.Reload(level.Find(co => co.ID == c.OwnerId));
                    }

                    var x = e.GridPosition.x;
                    var y = e.GridPosition.y;
                    var tile = DataStorage.savedData.Levels[i].Tiles[x][y];
                    tile.Add(e);

                    // Also update player
                    if (e.ID == DataStorage.savedData.Player.ID)
                    {
                        DataStorage.savedData.Player = e;
                    }
                }
                i++;
            }
        }

        private static void InitTiles()
        {
            for (int x = 0; x < Constants.MapWidth; x++)
            {
                for (int y = 0; y < Constants.MapHeight; y++)
                {
                    var tile = DataStorage.savedData.Levels[DataStorage.savedData.CurrentLevel].Tiles[x][y];
                    var matchingTile = GameObject.Find(string.Format("Tile: {0}, {1}", x, y));
                    var renderer = matchingTile.GetComponent<TileRenderer>();
                    tile.Renderer = renderer;
                    tile.NeedsUpdate = true;
                }
            }
        }

        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static T DeserializeObject<T>(string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringReader textReader = new StringReader(toDeserialize))
            {
                var o = xmlSerializer.Deserialize(textReader);
                return (T)o;
            }
        }
    }
}
