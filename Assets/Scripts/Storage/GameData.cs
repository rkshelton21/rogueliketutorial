﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class GameData
    {
        public Entity Player { get; set; }
        public List<LevelData> Levels { get; set; }
        public GameState.State CurrentState { get; set; }
        public int CurrentLevel { get; set; }
        public List<MessageSystem.MessageInfo> MessageQueue { get; set; }
        public List<List<Entity>> LevelEntities { get; set; }
    }
}
