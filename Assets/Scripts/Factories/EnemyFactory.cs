﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Spells;
using ED = Assets.Scripts.EntityDistribution;

namespace Assets.Scripts.Factories
{
    public class EnemyFactory
    {
        private static List<ED.DistributionDescription> _enemyList;
        private enum EnemyType
        {
            Troll,
            Orc
        }

        private static void Init()
        {
            if (_enemyList != null) return;

            _enemyList = new List<ED.DistributionDescription>();
            _enemyList.Add(new ED.DistributionDescription((char)EnemyType.Orc, ED.DV(0, 80)));
            _enemyList.Add(new ED.DistributionDescription((char)EnemyType.Troll, ED.DV(2, 15), ED.DV(4, 30), ED.DV(6, 60)));
        }

        public static Entity SpawnEnemy(int depth, Vector2Int location)
        {
            Init();

            var enemyType = EntityDistribution.GetEntity(depth, _enemyList);

            var entity = new Entity();
            var enemyController = new AIControllerComponent();
            entity.Depth = depth;
            entity.Display.Color = ColorLibrary.Colors.Enemy;
            entity.Display.Background = ColorLibrary.Colors.Null;
            entity.GridPosition = new Vector2Int(location.x, location.y);
            entity.AddComonent(enemyController);

            switch (enemyType)
            {
                case (char)EnemyType.Orc:
                    entity.Display.Representation = CharacterLibrary.Characters.EntitySettings.Orc;
                    entity.Name = "orc";
                    entity.AddComonent(new Combatant()
                    {
                        Damage = 4,
                        Defense = 0,
                        HP = 20,
                        MaxHP = 20,
                        XP = 35
                    });
                    break;
                case (char)EnemyType.Troll:
                    entity.Display.Representation = CharacterLibrary.Characters.EntitySettings.Troll;
                    entity.Name = "troll";
                    entity.AddComonent(new Combatant()
                    {
                        Damage = 8,
                        Defense = 2,
                        HP = 30,
                        MaxHP = 30,
                        XP = 100
                    });
                    break;
            }

            //XPUtilities.SetXP(entity);
            ControllerSystem.Register(enemyController);

            return entity;
        }
    }
}
