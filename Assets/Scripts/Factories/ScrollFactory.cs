﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Spells;
using ED = Assets.Scripts.EntityDistribution;

namespace Assets.Scripts.Factories
{
    public static class ScrollFactory
    {
        private static List<EntityDistribution.DistributionDescription> _scrollList;

        private static void Init()
        {
            if (_scrollList != null) return;

            _scrollList = new List<EntityDistribution.DistributionDescription>();
            _scrollList.Add(new EntityDistribution.DistributionDescription((char)Constants.ScrollType.Fireball, ED.DV(3, 25)));
            _scrollList.Add(new EntityDistribution.DistributionDescription((char)Constants.ScrollType.Lightning, ED.DV(5, 25)));
            _scrollList.Add(new EntityDistribution.DistributionDescription((char)Constants.ScrollType.Confusion, ED.DV(1, 10)));
        }


        public static Entity SpawnScroll(int depth, Vector2Int location)
        {
            Init();

            var scrollType = (Constants.ScrollType)(int)EntityDistribution.GetEntity(depth, _scrollList);

            switch (scrollType)
            {
                case Constants.ScrollType.Fireball:
                    return new Spell_Fireball().SpawnScroll(location);
                case Constants.ScrollType.Lightning:
                    return new Spell_Lightning().SpawnScroll(location);
                case Constants.ScrollType.Confusion:
                    return new Spell_Confuse().SpawnScroll(location);
            }

            return null;
        }
    }
}
