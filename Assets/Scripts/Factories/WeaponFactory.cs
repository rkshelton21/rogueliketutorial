﻿using Assets.Scripts.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ED = Assets.Scripts.EntityDistribution;

namespace Assets.Scripts.Factories
{
    public static class WeaponFactory
    {
        private static List<EntityDistribution.DistributionDescription> _weaponList;

        private static void Init()
        {
            if (_weaponList != null) return;

            _weaponList = new List<EntityDistribution.DistributionDescription>();
            _weaponList.Add(new EntityDistribution.DistributionDescription((char)Constants.WeaponType.Sword, ED.DV(0, 50)));
            _weaponList.Add(new EntityDistribution.DistributionDescription((char)Constants.WeaponType.Shield, ED.DV(3, 25)));
        }


        public static Entity SpawnWeapon(int depth, Vector2Int location)
        {
            Init();

            var weaponType = (Constants.WeaponType)(int)EntityDistribution.GetEntity(depth, _weaponList);

            switch (weaponType)
            {
                case Constants.WeaponType.Sword:
                    return new Weapon_Sword().SpawnWeapon(location);
                case Constants.WeaponType.Shield:
                    return new Weapon_Shield().SpawnWeapon(location);
            }

            return null;
        }
    }
}
