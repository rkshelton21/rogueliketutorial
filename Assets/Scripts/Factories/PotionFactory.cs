﻿using Assets.Scripts.Components;
using Assets.Scripts.Potions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ED = Assets.Scripts.EntityDistribution;

namespace Assets.Scripts.Factories
{
    public class PotionFactory
    {
        private static List<EntityDistribution.DistributionDescription> _potionList;
        private enum PotionType
        {
            Health
        }

        private static void Init()
        {
            if (_potionList != null) return;

            _potionList = new List<EntityDistribution.DistributionDescription>();
            _potionList.Add(new EntityDistribution.DistributionDescription((char)PotionType.Health, ED.DV(0, 35)));
        }

        public static Entity SpawnPotion(int depth, Vector2Int location)
        {
            return new Potion_Health().SpawnPotion(location);
        }
    }
}
