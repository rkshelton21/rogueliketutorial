﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public enum AIBehavior
    {
        Seeking,
        Fleeing,
        Hunting,
        Wandering,
        Sleeping,
        Dead,
        Idle,
        NotSet
    }

    public static class Constants
    {
        public enum PotionType
        {
            Health,
            Poison
        }

        public enum ScrollType
        {
            Lightning,
            Fireball,
            Confusion
        }

        public enum WeaponType
        {
            Sword,
            Shield
        }

        public const int PlayerViewDistance = 10;
        public const int MapWidth = 80;
        public const int MapHeight = 50;
        public const int XPScale = 100;

        public const int FullWidth = 40;
        public const int FullHeight = 25;
        public const int MapRenderWidth = 20;
        public const int MapRenderHeight = 20;
        public const int MessageHeight = 5;
        public const int OffsetX = 0;
        public const int OffsetY = 0;
        public const int DialogBuffer = 4;
        public const int DetailsHeight = 3;

        public const int ControllerButtonSize = 2;
        public const int InventoryButtonPos = ControllerButtonSize * 3 + ContollerPos + 1;
        public const int ContollerPos = 11;

        //public static RectInt StatsPanel = new RectInt(0, 0, 20, Constants.MapHeight);
        public static RectInt StatsPanel = new RectInt(OffsetX, OffsetY + MessageHeight, FullWidth - MapRenderWidth, FullHeight - MessageHeight);
        public static RectInt MapRender = new RectInt(OffsetX + FullWidth - MapRenderWidth, OffsetY + MessageHeight, MapRenderWidth, MapRenderHeight);
        public static RectInt MessageLayout = new RectInt(OffsetX, OffsetY, FullWidth, MessageHeight);

        public static RectInt MainMenu = new RectInt(OffsetX + DialogBuffer, OffsetY + DialogBuffer, FullWidth - DialogBuffer*2, FullHeight - DialogBuffer*2);
        public static RectInt DetailsDialog = new RectInt(OffsetX + DialogBuffer, OffsetY + DialogBuffer + DetailsHeight, FullWidth - DialogBuffer * 2, FullHeight - DialogBuffer * 2 - DetailsHeight);
        public static RectInt DetailsOptions = new RectInt(OffsetX + DialogBuffer, OffsetY + DialogBuffer, FullWidth - DialogBuffer * 2, DetailsHeight);
        public static RectInt InventoryDialog = new RectInt(OffsetX + DialogBuffer, OffsetY + DialogBuffer, FullWidth - DialogBuffer * 2, FullHeight - DialogBuffer * 2);
        public static RectInt DebugDialog = new RectInt(OffsetX + DialogBuffer, OffsetY + DialogBuffer, FullWidth - DialogBuffer * 2, FullHeight - DialogBuffer * 2);
    }
}
