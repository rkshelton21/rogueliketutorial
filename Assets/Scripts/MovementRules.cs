﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class MovementRules
    {
        public enum MovementStatus
        {
            Blocked,
            Occupied,
            Valid,
            Idle,
            Invalid,
            NotSet
        }

        public static MovementStatus AllowMove(Vector2Int destination, int depth)
        {
            var t = MapSystem.Tile(destination.x, destination.y, depth);
            if (t.Walkable)
            {
                if (t.Occupied)
                {
                    return MovementStatus.Occupied;
                }
                else
                {
                    return MovementStatus.Valid;
                }
            }
            else
            {
                if(t.TType == RLTile.TileType.Stairs)
                {
                    if(t.Location == MapSystem.CurrentLevel.StairsUp)
                    {
                        MapSystem.PreviousLevel();
                    }
                    if (t.Location == MapSystem.CurrentLevel.StairsDown)
                    {
                        MapSystem.NextLevel();
                    }
                }
                return MovementStatus.Blocked;
            }
        }
    }
}
