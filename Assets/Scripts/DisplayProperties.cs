﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DisplayProperties
{
    public char Representation;
    public Color Color;
    public Color Background;
}
