﻿using Assets.Scripts.Components;
using Assets.Scripts.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class EntitySpawner
    {
        private static System.Random _random = new System.Random(1);

        public static void PlaceEntities(LevelData level, RectInt room)
        {
            var maxMonstersPerRoom = EntityDistribution.MaxMonstersPerRoom(level.Depth);
            var maxItemsPerRoom = EntityDistribution.MaxItemsPerRoom(level.Depth);
            //Get a random number of monsters
            var monsterCount = _random.Next(0, maxMonstersPerRoom + 1);
            var itemCount = _random.Next(0, maxItemsPerRoom + 1);

            for (int i=0; i<monsterCount; i++)
            {
                var t = AttemptRandomTile(level, room);
                if (!t.Occupied)
                {
                    var e = EnemyFactory.SpawnEnemy(t.Depth, t.Location);
                    t.Add(e);
                }
            }

            for (int i = 0; i < itemCount; i++)
            {
                var t = AttemptRandomTile(level, room);
                if (!t.HasItem)
                {
                    SpawnItem(t);
                }
            }
        }

        public static Entity SpawnPlayer(Vector2Int location)
        {
            var player = new Entity(CharacterLibrary.Characters.EntitySettings.Player);
            var playerController = new PlayerControllerComponent();
            player.Display.Color = ColorLibrary.Colors.Player;
            player.GridPosition = location;
            player.AddComonent(playerController);
            player.AddComonent(new InventoryComponent());
            player.AddComonent(new Combatant()
            {
                CType = ComponentType.Combatant,
                Damage = 4,
                Defense = 1,
                HP = 100,
                MaxHP = 100
            });

            ControllerSystem.Player = playerController;

            return player;
        }

        private static void SpawnItem(RLTile tile)
        {
            var type = EntityDistribution.GetItem(tile.Depth);
            Entity entity = null;
            if (type == CharacterLibrary.Characters.EntitySettings.Potion)
            {
                entity = PotionFactory.SpawnPotion(tile.Depth, tile.Location);
            }
            else if (type == CharacterLibrary.Characters.EntitySettings.Scroll)
            {
                entity = ScrollFactory.SpawnScroll(tile.Depth, tile.Location);
            }
            else if (type == CharacterLibrary.Characters.EntitySettings.Weapon)
            {
                entity = WeaponFactory.SpawnWeapon(tile.Depth, tile.Location);
            }

            tile.Add(entity);
        }

        private static RLTile AttemptRandomTile(LevelData level, RectInt room)
        {
            //Choose a random location in the room
            var x = _random.Next(0, room.width) + room.xMin;
            var y = _random.Next(0, room.height) + room.yMin;
            var t = level.Tiles[x][y];
            return t;
        }
    }
}
