﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class ModifierItemBase : AttributeModifier
    {
        public enum MessageType
        {
            None,
            PlayerModified,
            PlayerNotNeeded,
            TargetModified,
            TargetNotNeeded
        }

        public virtual bool Apply(AttributeModifier modifier, Entity user, Entity target)
        {
            if (target == null) return false;

            if (modifier.Used)
            {
                Debug.LogError("Already used.");
                return false;
            }

            MessageType messageType = MessageType.None;
            messageType = GetRequiredAction(modifier, user, target);
            var message = GetFormattedMessage(messageType, modifier, user, target);
            if(!String.IsNullOrEmpty(message))
                MessageSystem.AddMessage(message, ColorLibrary.Colors.Message_SpellText);
            modifier.ApplyToEntity(user, target);

            switch (messageType)
            {
                case MessageType.None:
                    Debug.LogError("Hmmmm");
                    return false;
                case MessageType.PlayerNotNeeded:
                    return false;
                case MessageType.TargetNotNeeded:
                    return false;
            }

            modifier.Used = true;
            return true;
        }

        protected virtual string GetFormattedMessage(MessageType messageType, AttributeModifier modifier, Entity user, Entity target)
        {
            var message = GetMessage(modifier.TypeId, messageType);
            message = FormatMessage(message, modifier, user, target);
            return message;
        }

        protected virtual MessageType GetRequiredAction(AttributeModifier modifier, Entity user, Entity target)
        {
            throw new NotImplementedException();
        }

        protected virtual string GetMessage(int itemTypeId, MessageType messageType)
        {
            throw new NotImplementedException();
        }

        private string FormatMessage(string message, AttributeModifier modifier, Entity user, Entity target)
        {
            return message.Replace("{T}", target.Name).Replace("{U}", user.Name).Replace("{N}", ((int)modifier.Ammount).ToString());
        }
    }
}
