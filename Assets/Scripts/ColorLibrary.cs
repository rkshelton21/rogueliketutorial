﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class ColorLibrary
    {
        private static ColorLibrary _Colors = null;

        public static ColorLibrary Colors
        {
            get
            {
                if (_Colors == null)
                {
                    _Colors = DataStorage.LoadColorLibrary();
                    if (_Colors == null)
                        _Colors = new ColorLibrary();
                }
                return _Colors;
            }
            set
            {
                _Colors = value;
            }
        }

        public ColorLibrary()
        {
            Wall_Dark = new Color(0.39f, 0.39f, 0.39f);
            Wall_DarkB = new Color(0, 0, 0.39f);
            Wall_Light = new Color(0.5f, 0.4f, 0.196f);
            Wall_LightB = new Color(0.7f, 0.4f, 0.196f);
            Wall_Stairs_Up = new Color(0.76f, 0.08f, 0.0f);
            Wall_Stairs_Down = new Color(0.3f, 1f, 0.2f);

            Ground_Dark = new Color(0.196f, 0.196f, 0.588f);
            Ground_Light = new Color(0.784f, 0.7f, 0.196f);

            Hover = new Color(1f, 1f, 1f);
            HoverB = new Color(0f, 0.4f, 0.4f);

            DeadBody = new Color(0.48f, 0.1f, 0.1f);
            UIColor = new Color(0.8f, 0.8f, 0.8f);
            UIGlow = new Color(0.9f, 0.4f, 0.4f);
            UIBase = new Color(0.192f, 0.24f, 0.24f);
            UIBaseDark = new Color(0.092f, 0.14f, 0.14f);
            DialogBackground = new Color(0.092f, 0.14f, 0.092f);
            HealthBar = new Color(0.88f, 0.1f, 0.1f);

            Player = new Color(0.25f, 1f, 0.25f);
            Potion = new Color(0.11f, 0.7f, 1f);
            Enemy = new Color(1f, 0.41f, 0f);
            Null = new Color(1f, 0f, 1f, 0f);

            Message_Warning = new Color(0.98f, 0.62f, 0.03f);
            Message_SpellText = new Color(0.11f, 0.7f, 1f);
            Message_Damage = new Color(0.92f, 0.35f, 0.42f);
            Message_Info = new Color(1f, 1f, 1f);
            Message_StatBuff = new Color(0.25f, 1f, 0.25f);
        }

        public Color Wall_Dark { get; set; }
        public Color Wall_DarkB { get; set; }
        public Color Wall_Light { get; set; }
        public Color Wall_LightB { get; set; }
        public Color Wall_Stairs_Up { get; set; }
        public Color Wall_Stairs_Down { get; set; }

        public Color Ground_Dark { get; set; }
        public Color Ground_Light { get; set; }

        public Color Hover { get; set; }
        public Color HoverB { get; set; }

        public Color DeadBody { get; set; }
        public Color UIColor { get; set; }
        public Color UIGlow { get; set; }
        public Color UIBase { get; set; }
        public Color UIBaseDark { get; set; }
        public Color DialogBackground { get; set; }

        public Color HealthBar { get; set; }
        public Color Player { get; set; }
        public Color Enemy { get; set; }
        public Color Potion { get; set; }
        public Color Null { get; set; }

        public Color Message_Warning { get; set; }
        public Color Message_SpellText { get; set; }
        public Color Message_Damage { get; set; }
        public Color Message_Info { get; set; }
        public Color Message_StatBuff { get; set; }
    }
}
