﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class MapInfo
    {
        public List<LevelData> Levels = new List<LevelData>();
        public int CurrentDepth = 0;
        public Entity Player = null;

        public LevelData CurrentLevelData { get { return Levels[CurrentDepth]; } }
        public RLTile[][] CurrentTiles { get { return Levels[CurrentDepth].Tiles; } }

        public RLTile Tile(Vector2Int pos, int depth)
        {
            if (pos.x < 0 || pos.y < 0) return null;
            if (pos.x >= Constants.MapWidth || pos.y >= Constants.MapHeight) return null;

            if (depth < 0 || depth >= Levels.Count)
                Debug.LogError("Ooops");
            return Levels[depth].Tiles[pos.x][pos.y];
        }

        public static MapInfo CreateNewMap()
        {
            ControllerSystem.ReStart(null);
            var mapInfo = MapGenerator.GenerateMap();
            var location = mapInfo.CurrentLevelData.StairsUp;
            var player = EntitySpawner.SpawnPlayer(location);
            mapInfo.Player = player;
            mapInfo.Tile(player.GridPosition, player.Depth).Add(player);

            return mapInfo;
        }

        public static MapInfo CreateEmptyMap()
        {
            var mapInfo = MapGenerator.GenerateEmptyMap();
            return mapInfo;
        }

        public static MapInfo LoadMap()
        {
            DataStorage.Load(true);
            var data = DataStorage.savedData;

            var mapInfo = new MapInfo();
            mapInfo.Levels = data.Levels;
            mapInfo.CurrentDepth = data.CurrentLevel;
            mapInfo.Player = data.Player;

            ControllerSystem.Player = data.Player.GetComponent<PlayerControllerComponent>(ComponentType.Controller);
            var pos = data.Player.GridPosition;
            mapInfo.Tile(new Vector2Int(pos.x, pos.y), data.CurrentLevel).Add(data.Player);

            var allEntities = new List<Entity>();
            foreach(var level in data.LevelEntities) { allEntities.AddRange(level); }
            ControllerSystem.ReStart(allEntities);
            MessageSystem.SetMessage(data.MessageQueue);
            return mapInfo;
        }
    }
}
