﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class DebugRender
    {
        static RectInt MiddelPanel = Constants.DebugDialog;
        static List<string> _descriptions = new List<string>();

        public static void OpenDebugger()
        {
            UpdateDebugger();
        }

        public static void CloseDebugger()
        {
            DeRegisterTiles();
        }

        private static void UpdateDebugger()
        {
            var hoverTile = MouseSystem.GetHighlightedTile();
            _descriptions.Clear();
            _descriptions.Add(String.Format("Location: {0}, {1}", hoverTile.Location.x, hoverTile.Location.y));
            _descriptions.Add(String.Format("Occupied: {0}", hoverTile.Occupied));

            _descriptions.Add(String.Format("Display:"));
            var display = hoverTile.GetDisplay(false);
            _descriptions.Add(String.Format("       : {0} {1}", display.Representation, display.Color));
            _descriptions.Add(String.Format("       :   {0}", display.Background));

            _descriptions.Add(String.Empty);
            foreach (var e in hoverTile.GetAllEntities())
            {
                _descriptions.Add(String.Format("Entity: {0}", e.Name));
                var combatant = e.GetComponent<Combatant>(ComponentType.Combatant);
                var mod = e.GetComponent<AttributeModifier>(ComponentType.AttributeModifier);
                if(combatant != null)
                {
                    _descriptions.Add(String.Format("Char Info:"));
                    _descriptions.Add(String.Format("        HP:{0}/{1}", combatant.HP, combatant.MaxHP));
                    _descriptions.Add(String.Format("       DMG:{0} + {1}", combatant.Damage, combatant.DamageBonus));
                    _descriptions.Add(String.Format("       DEF:{0} + {1}", combatant.Defense, combatant.DefenseBonus));
                    _descriptions.Add(String.Format("       LVL:{0}", combatant.Level));
                    _descriptions.Add(String.Format("        XP:{0}", combatant.XP));
                    _descriptions.Add(String.Empty);
                }
                if(mod != null)
                {
                    _descriptions.Add(String.Format("Item Info:"));
                    _descriptions.Add(String.Format("       AMT:{0}", mod.Ammount));
                    _descriptions.Add(String.Format("       TGT:{0}", Enum.GetName(typeof(AttributeModifier.AttributeTarget), mod.Target)));
                    _descriptions.Add(String.Format("       RNG:{0}", mod.AffectedRange));
                    _descriptions.Add(String.Empty);
                }
            }

            for(int i=0; i<_descriptions.Count; i++)
            {
                _descriptions[i] = ("  " + _descriptions[i]).PadRight(MiddelPanel.width);
            }
            RegisterTiles();
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.RenderFunction = RenderDebuggerTile;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void RenderDebuggerTile(RLTile tile)
        {
            if (tile.TestData == "Q")
                tile.TestData = "Q";

            var panelY = MiddelPanel.yMin + MiddelPanel.height - tile.Location.y;
            var panelX = tile.Location.x - MiddelPanel.xMin;

            if (panelY < _descriptions.Count)
            {
                tile.Renderer.TextMesh.text = _descriptions[panelY][panelX].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
        }
    }
}
