﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class LeftPanelRender
    {
        public static float _cooldown = 0f;

        private static RectInt LeftPanel = Constants.StatsPanel;
        private static Combatant _combatant;
        private static float _maxHP;
        static RectInt _up = new RectInt((Constants.StatsPanel.width - Constants.ControllerButtonSize * 3) / 2 + Constants.ControllerButtonSize, Constants.ContollerPos, Constants.ControllerButtonSize - 1, Constants.ControllerButtonSize - 1);
        static RectInt _down = new RectInt((Constants.StatsPanel.width - Constants.ControllerButtonSize * 3) / 2 + Constants.ControllerButtonSize, Constants.ContollerPos + Constants.ControllerButtonSize * 2, Constants.ControllerButtonSize - 1, Constants.ControllerButtonSize - 1);
        static RectInt _left = new RectInt((Constants.StatsPanel.width - Constants.ControllerButtonSize * 3) / 2, Constants.ContollerPos + Constants.ControllerButtonSize, Constants.ControllerButtonSize - 1, Constants.ControllerButtonSize - 1);
        static RectInt _right = new RectInt((Constants.StatsPanel.width - Constants.ControllerButtonSize * 3) / 2 + Constants.ControllerButtonSize * 2, Constants.ContollerPos + Constants.ControllerButtonSize, Constants.ControllerButtonSize - 1, Constants.ControllerButtonSize - 1);
        public static void ReserveAllTiles(MapInfo info)
        {
            for (int l = 0; l < info.Levels.Count; l++)
            {
                for (int x = 0; x < LeftPanel.width; x++)
                {
                    for (int y = 0; y < LeftPanel.height; y++)
                    {
                        var tile = info.Levels[l].Tiles[LeftPanel.xMin + x][LeftPanel.yMin + LeftPanel.height - 1 - y];
                        tile.ReservedForUI = true;
                    }
                }
            }
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < LeftPanel.width; x++)
            {
                for (int y = 0; y < LeftPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(LeftPanel.xMin + x, LeftPanel.yMin + LeftPanel.height - 1 - y);
                    tile.ReservedForUI = true;
                    tile.NeedsUpdate = true;
                    tile.RenderFunction = RenderLeftPanelTile;

                    if (y == 2)
                    {
                        tile.RenderFunction = RenderHealthTile;
                        tile.TestData = "Q";
                    }
                    if (y == 4)
                        tile.RenderFunction = RenderXPTile;
                    if (y == 6)
                        tile.RenderFunction = RenderDepthTile;
                    if (y == 8)
                        tile.RenderFunction = RenderLevelTile;

                    if((y >= _up.yMin && y <= _up.yMax) && (x >= _up.xMin && x <= _up.xMax))
                        tile.RenderFunction = RenderArrowUpTile;
                    if ((y >= _down.yMin && y <= _down.yMax) && (x >= _down.xMin && x <= _down.xMax))
                        tile.RenderFunction = RenderArrowDownTile;
                    if ((y >= _left.yMin && y <= _left.yMax) && (x >= _left.xMin && x <= _left.xMax))
                        tile.RenderFunction = RenderArrowLeftTile;
                    if ((y >= _right.yMin && y <= _right.yMax) && (x >= _right.xMin && x <= _right.xMax))
                        tile.RenderFunction = RenderArrowRightTile;

                    if (y == Constants.InventoryButtonPos)
                        tile.RenderFunction = RenderInvButtonTile;

                    tile.DefaultRenderFunction = tile.RenderFunction;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < LeftPanel.width; x++)
            {
                for (int y = 0; y < LeftPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(x, y);
                    tile.RenderFunction = null;
                }
            }
        }

        public static void UpdatePanel(Combatant playerCombatant, float playerMaxHP)
        {
            _combatant = playerCombatant;
            _maxHP = playerMaxHP;

            //RenderHealth(playerCombatant, playerMaxHP);
            //RenderDepth(ControllerSystem.Player.Owner);
            //RenderPlayerLevel(playerCombatant);
        }

        private static void RenderHealthTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            Vector2Int v = new Vector2Int(LeftPanel.xMin + 1, LeftPanel.yMax - 2);
            int width = 18;
            var renderWidth = (int)(_combatant.HP * (width / _maxHP));
            var barStart = v.x;
            var healthEnd = barStart + renderWidth;
            var barEnd = barStart + width;
            var barMsg = String.Format("HP: {0}/{1}", Mathf.CeilToInt(_combatant.HP), (int)_combatant.MaxHP);
            var padding = width - barMsg.Length;
            barMsg = barMsg.PadLeft(padding / 2 + barMsg.Length, ' ').PadRight(width + 1, ' ');

            if (tile.Location.x >= barStart && tile.Location.x <= healthEnd)
            {
                //tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelHealth.ToString();
                tile.Renderer.TextMesh.text = barMsg[tile.Location.x - barStart].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.Wall_Dark;
                tile.Renderer.Background.color = ColorLibrary.Colors.HealthBar;
            }
            else if(tile.Location.x >= barStart && tile.Location.x <= barEnd)
            {
                //tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelHealthMissing.ToString();
                tile.Renderer.TextMesh.text = barMsg[tile.Location.x - barStart].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
        }

        private static void RenderInvButtonTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            Vector2Int v = new Vector2Int(LeftPanel.xMin + 1, LeftPanel.yMax - 2);
            int width = 18;
            var buttonStart = v.x;
            var barEnd = buttonStart + width;
            var barMsg = "Inventory";
            var padding = width - barMsg.Length;
            barMsg = barMsg.PadLeft(padding / 2 + barMsg.Length, ' ').PadRight(width + 1, ' ');

            if (tile.Location.x >= buttonStart && tile.Location.x <= barEnd)
            {
                //tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelHealthMissing.ToString();
                tile.Renderer.TextMesh.text = barMsg[tile.Location.x - buttonStart].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
        }

        private static void RenderXPTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            Vector2Int v = new Vector2Int(LeftPanel.xMin + 1, LeftPanel.yMax - 2);
            int width = 18;
            var renderWidth = (int)(((float)_combatant.XP) * ((float)width / ((float)_combatant.Level * Constants.XPScale)));
            var barStart = v.x;
            var xpEnd = barStart + renderWidth;
            var barEnd = barStart + width;
            var barMsg = String.Format("XP: {0}/{1}", Mathf.CeilToInt(_combatant.XP), (int)(_combatant.Level * Constants.XPScale));
            var padding = width - barMsg.Length;
            barMsg = barMsg.PadLeft(padding / 2 + barMsg.Length, ' ').PadRight(width + 1, ' ');

            if (tile.Location.x >= barStart && tile.Location.x <= xpEnd)
            {
                tile.Renderer.TextMesh.text = barMsg[tile.Location.x - barStart].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.Wall_Dark;
                tile.Renderer.Background.color = ColorLibrary.Colors.Potion;
            }
            else if (tile.Location.x >= barStart && tile.Location.x <= barEnd)
            {
                tile.Renderer.TextMesh.text = barMsg[tile.Location.x - barStart].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
        }

        private static void RenderLeftPanelTile(RLTile tile)
        {
            tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
            tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
        }

        private static void RenderDepthTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (ControllerSystem.Player == null) return;

            var renderMessage = String.Format("CurrentDepth: {0}", ControllerSystem.Player.Owner.Depth + 1);
            Vector2Int v = new Vector2Int(LeftPanel.xMin + 1, LeftPanel.yMax - 4);
            var xMin = v.x;
            var xMax = xMin + renderMessage.Length;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            if (tile.Location.x >= xMin && tile.Location.x < xMax)
            {
                tile.Renderer.TextMesh.text = renderMessage[tile.Location.x - xMin].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
            }
        }

        private static void RenderLevelTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            Vector2Int v = new Vector2Int(LeftPanel.xMin + 1, LeftPanel.yMax - 6);
            var renderMessage = String.Format("You are level: {0}", _combatant.Level);
            var xMin = v.x;
            var xMax = xMin + renderMessage.Length;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            if (tile.Location.x >= xMin && tile.Location.x < xMax)
            {
                tile.Renderer.TextMesh.text = renderMessage[tile.Location.x - xMin].ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.UISettings.LeftPanelBackground.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
            }
        }

        private static void RenderArrowUpTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            tile.Renderer.TextMesh.text = '^'.ToString();
            tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
        }

        private static void RenderArrowLeftTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            tile.Renderer.TextMesh.text = '<'.ToString();
            tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
        }

        private static void RenderArrowRightTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            tile.Renderer.TextMesh.text = '>'.ToString();
            tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
        }

        private static void RenderArrowDownTile(RLTile tile)
        {
            tile.NeedsUpdate = true;
            if (_combatant == null) return;

            tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            tile.Renderer.TextMesh.text = 'v'.ToString();
            tile.Renderer.TextMesh.color = ColorLibrary.Colors.HealthBar;
        }

        public static void ProcessInput()
        {
            _cooldown -= Time.deltaTime;
            if (GameState.CurrentState != GameState.State.PlayerMoving) return;

            var highlight = MouseSystem.GetHighlightedTile();
            if (highlight != null)
            {
                var hPos = highlight.Location;
                var panelY = LeftPanel.yMin + LeftPanel.height - hPos.y - 1;
                var panelX = hPos.x - LeftPanel.xMin;
                var xInRange = hPos.x > LeftPanel.xMin + 2 && hPos.x < LeftPanel.xMax - 2;
                var yInRange = panelY == Constants.InventoryButtonPos || (panelY >= Constants.ContollerPos && panelY <= Constants.ContollerPos + Constants.ControllerButtonSize * 3);
                var touch = Input.touchCount > 0 ? true : false;
                var tap = touch ? Input.touches[0].phase == TouchPhase.Began : false;

                if (xInRange && yInRange)
                {
                    var inventoryIndex = LeftPanel.yMax - hPos.y;

                    // highlight row
                    for (int x = 0; x < LeftPanel.width; x++)
                    {
                        var t = MapSystem.CurrentLevelTile(LeftPanel.xMin + x, hPos.y);
                        //t.Highlighted = true;
                    }

                    if (touch || Input.GetMouseButtonDown(0))
                    {
                        if(panelY == Constants.InventoryButtonPos && (!touch || (touch && tap)))
                        {
                            GameState.ToggleInventory();
                        }

                        var moveX = 0;
                        var moveY = 0;
                        if ((panelY >= _up.yMin && panelY <= _up.yMax) && (panelX >= _up.xMin && panelX <= _up.xMax))
                            moveY = 1;
                        if ((panelY >= _down.yMin && panelY <= _down.yMax) && (panelX >= _down.xMin && panelX <= _down.xMax))
                            moveY = -1;
                        if ((panelY >= _left.yMin && panelY <= _left.yMax) && (panelX >= _left.xMin && panelX <= _left.xMax))
                            moveX = -1;
                        if ((panelY >= _right.yMin && panelY <= _right.yMax) && (panelX >= _right.xMin && panelX <= _right.xMax))
                            moveX = 1;

                        if(_cooldown < 0f && (moveX != 0 || moveY != 0))
                        {
                            _cooldown = ControllerSystem.MovementDelay;
                            var moveStatus = ControllerSystem.Player.Move(new Vector2Int(moveX, moveY));
                            if (moveStatus == MovementRules.MovementStatus.Occupied || moveStatus == MovementRules.MovementStatus.Valid)
                            {
                                //_cooldown = MovementDelay;
                                if (GameState.CurrentState == GameState.State.PlayerMoving)
                                {
                                    GameState.CurrentState = GameState.State.MonstersMoving;
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
