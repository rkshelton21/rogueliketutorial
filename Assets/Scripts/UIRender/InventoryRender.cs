﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class InventoryRender
    {
        static RectInt MiddelPanel = Constants.InventoryDialog;
        static int _highlightedRowIndex = -1;
        static int _highlightedRowY = -1;
        static int _previouslyHighlightedRowY = -1;
        static bool _inventoryOpen = false;
        static List<ItemComponent> _items;
        static List<string> _descriptions;

        public static void OpenInventory()
        {
            _inventoryOpen = true;
            UpdateItems();
        }

        public static void CloseInventory()
        {
            _inventoryOpen = false;
            DeRegisterTiles();
            _items = null;
            _descriptions = null;
        }

        private static void UpdateItems()
        {
            _items = ControllerSystem.Player.Owner.GetComponent<InventoryComponent>(ComponentType.Inventory).Items;
            _descriptions = new List<string>();
            var i = 0;
            foreach (var item in _items)
            {
                var msg = item.Owner.Name;
                if (item.Equipped)
                    msg += " (Equipped)";

                var desc = String.Format("{0}{1}) {2}", CharacterLibrary.Characters.UISettings.InventoryBuffer_Front, i, msg);
                _descriptions.Add(desc.PadRight(MiddelPanel.width - CharacterLibrary.Characters.UISettings.InventoryBuffer_Back.Length) + CharacterLibrary.Characters.UISettings.InventoryBuffer_Back);
                i++;
            }
            RegisterTiles();
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.RenderFunction = RenderInventoryTile;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void ProcessInput()
        {
            if (!_inventoryOpen) return;

            ProcessForInventoryMenu();
        }


        private static void ProcessForInventoryMenu()
        {
            _highlightedRowY = -1;
            _highlightedRowIndex = -1;

            var highlight = MouseSystem.GetHighlightedTile();
            if (highlight != null)
            {
                var hPos = highlight.Location;
                var xInRange = hPos.x > MiddelPanel.xMin + 3 && hPos.x < MiddelPanel.xMax - 3;
                var yInRange = hPos.y > MiddelPanel.yMax - _items.Count && hPos.y <= MiddelPanel.yMax;

                if (xInRange && yInRange)
                {
                    var inventoryIndex = MiddelPanel.yMax - hPos.y;
                    _highlightedRowY = hPos.y;
                    _highlightedRowIndex = inventoryIndex;

                    // highlight row
                    for (int x = 0; x < MiddelPanel.width; x++)
                    {
                        var t = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, hPos.y);
                        //t.Highlighted = true;
                    }
                }
            }

            if (Input.GetMouseButtonDown(0) && _highlightedRowIndex >= 0)
            {
                var modifier = _items[_highlightedRowIndex].Owner.GetComponent<AttributeModifier>(ComponentType.AttributeModifier);
                if (modifier == null)
                {
                    MessageSystem.AddMessage("I don't know how to use that yet.", ColorLibrary.Colors.Message_Warning);
                }
                else
                {
                    var selectedItem = _items[_highlightedRowIndex].Owner;
                    GameState.ToggleItemDetails(selectedItem);
                }
            }
        }


        public static void RenderInventoryTile(RLTile tile)
        {
            if (tile.TestData == "Q")
                tile.TestData = "Q";

            var panelY = MiddelPanel.yMin + MiddelPanel.height - tile.Location.y;
            var panelX = tile.Location.x - MiddelPanel.xMin;
            if (tile.Location.y == _previouslyHighlightedRowY)
                tile.NeedsUpdate = true;

            if (panelY < _items.Count)
            {
                if (tile.Location.y == _highlightedRowY)
                {
                    tile.Renderer.TextMesh.text = _descriptions[panelY][panelX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.UIGlow;
                }
                else
                {
                    tile.Renderer.TextMesh.text = _descriptions[panelY][panelX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
                }
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
            _previouslyHighlightedRowY = _highlightedRowY;
        }
    }
}
