﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class TargetingRender
    {
        private static Entity _entity;
        private static AttributeModifier _modifier;
        private static ItemComponent _item;
        private static bool _targeting = false;
        private static int _targetingRange = 0;

        public static void OpenTargeting(Entity item)
        {
            _entity = item;
            _modifier = item.GetComponent<AttributeModifier>(ComponentType.AttributeModifier);
            _item = item.GetComponent<ItemComponent>(ComponentType.Item);
            _targetingRange = _modifier.GetVisibleTargetRange();
            _targeting = true;
        }

        public static void CloseTargeting()
        {
            _targeting = false;
            _entity = null;
            _modifier = null;
            _item = null;
        }

        public static void ProcessInput()
        {
            if (!_targeting) return;

            if (Input.GetMouseButtonDown(0))
            {
                if (_modifier == null)
                {
                    MessageSystem.AddMessage("I don't know how to do that yet.", ColorLibrary.Colors.Message_Warning);
                }
                else
                {
                    var used = _modifier.UseModifier();
                    if (_modifier.Used)
                    {
                        ControllerSystem.Player.Owner.GetComponent<InventoryComponent>(ComponentType.Inventory).Items.Remove(_item);
                    }
                    if (used)
                    {
                        GameState.ToggleTargeting(_entity);
                        GameState.ToggleInventory();
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameState.ToggleTargeting(_entity);
            }
        }

        public static int GetVisibleTargetingRange()
        {
            return _targetingRange;
        }

        private static List<RLTile> GetTargetedTiles()
        {
            var results = new List<RLTile>();
            var center = MouseSystem.GetHighlightedTile();
            results.Add(center);
            results.AddRange(MappingUtilities.Neighbors(center));
            return results;
        }
    }
}
