﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class DisplayMapRender
    {
        static RectInt MiddelPanel = Constants.MapRender;
        static List<string> _descriptions = new List<string>();
        static Vector2Int _center = new Vector2Int();
        static Vector2Int _offset = new Vector2Int();

        public static void OpenMap()
        {
            RegisterTiles();
        }

        public static void CloseMap()
        {
            DeRegisterTiles();
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y - 1);
                    tile.RenderFunction = RenderMapTile;
                    tile.DefaultRenderFunction = tile.RenderFunction;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y - 1);
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void RenderMapTile(RLTile tile)
        {
            var panelY = tile.Location.y - MiddelPanel.yMin;
            var panelX = tile.Location.x - MiddelPanel.xMin;
            _offset = ControllerSystem.Player.Owner.GridPosition - new Vector2Int(MiddelPanel.width / 2, MiddelPanel.height / 2);

            var tilePos = _offset + new Vector2Int(panelX, panelY);
            var actualTile = MapSystem.CurrentLevelTile(tilePos.x, tilePos.y);
            if (actualTile != null)
            {
                var display = actualTile.GetDisplay();

                tile.Renderer.TextMesh.text = display.Representation.ToString();
                tile.Renderer.TextMesh.color = display.Color;
                tile.Renderer.Background.color = display.Background;

                actualTile.ModifyRenderer(tile);
            }
            else
            {
                tile.Renderer.TextMesh.text = CharacterLibrary.Characters.EnvironmentSettings.Wall.ToString();
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.Wall_Dark;
                tile.Renderer.Background.color = ColorLibrary.Colors.Wall_DarkB;
            }
        }

        public static RLTile GetRenderMapTile(Vector2Int position)
        {
            var panelY = position.y - MiddelPanel.yMin;
            var panelX = position.x - MiddelPanel.xMin;

            _offset = ControllerSystem.Player.Owner.GridPosition - new Vector2Int(MiddelPanel.width / 2, MiddelPanel.height / 2);

            var tilePos = _offset + new Vector2Int(panelX, panelY);
            var actualTile = MapSystem.CurrentLevelTile(tilePos.x, tilePos.y);
            return actualTile;
        }
    }
}
