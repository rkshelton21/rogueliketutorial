﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public class MainMenuRender
    {
        private const string NewGameOption = "New Game";
        private const string ContinueOption = "Continue";
        private static bool _Enabled = false;
        static RectInt MiddelPanel = Constants.MainMenu;
        static int _highlightedRowIndex = -1;
        static int _highlightedRowY = -1;
        static int _previouslyHighlightedRowY = -1;
        static List<string> _menuItemDescriptions = new List<string>() {
            String.Empty.PadRight(MiddelPanel.width, ' '),
            String.Empty.PadRight(MiddelPanel.width, ' '),
            NewGameOption.PadLeft(MiddelPanel.width/2 + NewGameOption.Length/2).PadRight(MiddelPanel.width),
            String.Empty.PadRight(MiddelPanel.width, ' '),
            ContinueOption.PadLeft(MiddelPanel.width/2 + ContinueOption.Length/2).PadRight(MiddelPanel.width),
        };

        private static void ModifyDisplay(RLTile t)
        {
            var delta = 0.01f;

            var n = MapSystem.CurrentLevelTile(t.Location.x, t.Location.y + 1);
            var s = MapSystem.CurrentLevelTile(t.Location.x, t.Location.y - 1);
            var e = MapSystem.CurrentLevelTile(t.Location.x + 1, t.Location.y);
            var w = MapSystem.CurrentLevelTile(t.Location.x - 1, t.Location.y);

            var minR = 1f;
            var maxR = 0f;
            var minG = 1f;
            var maxG = 0f;
            var minB = 1f;
            var maxB = 0f;
            RLTile t2 = null;
            var r = new System.Random();
            var d = r.Next(0, 4);
            switch(d)
            {
                case 0:
                    t2 = n;
                    break;
                case 1:
                    t2 = s;
                    break;
                case 2:
                    t2 = e;
                    break;
                case 3:
                    t2 = w;
                    break;
            }

            if (t2 == null)
                return;

            minR = Mathf.Max(0f, (t.DefaultDisplay.Background.r + t2.DefaultDisplay.Background.r)/2 - delta);
            maxR = Mathf.Min(1f, (t.DefaultDisplay.Background.r + t2.DefaultDisplay.Background.r) / 2 + delta);
            minG = Mathf.Max(0f, (t.DefaultDisplay.Background.g + t2.DefaultDisplay.Background.g) / 2 - delta);
            maxG = Mathf.Min(1f, (t.DefaultDisplay.Background.g + t2.DefaultDisplay.Background.g) / 2 + delta);
            minB = Mathf.Max(0f, (t.DefaultDisplay.Background.b + t2.DefaultDisplay.Background.b) / 2 - delta);
            maxB = Mathf.Min(1f, (t.DefaultDisplay.Background.b + t2.DefaultDisplay.Background.b) / 2 + delta);

            var red = (float)(r.NextDouble()*(maxR - minR) + minR);
            var green = (float)(r.NextDouble() * (maxG - minG) + minG);
            var blue = (float)(r.NextDouble() * (maxB - minB) + minB);
            t.DefaultDisplay.Background = new Color(red, green, blue);
            t.DefaultDisplay.Color = new Color(1f, 1f, 1f, 1f);
        }

        public static void RegisterTiles()
        {
            _Enabled = true;
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.RenderFunction = RenderMainMenuTile;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            _Enabled = false;
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void RenderMainMenuTile(RLTile tile)
        {
            var panelY = MiddelPanel.yMin + MiddelPanel.height - tile.Location.y;
            var panelX = tile.Location.x - MiddelPanel.xMin;
            if (tile.Location.y == _previouslyHighlightedRowY)
                tile.NeedsUpdate = true;

            if (panelY < _menuItemDescriptions.Count)
            {
                if (tile.Location.y == _highlightedRowY)
                {
                    tile.Renderer.TextMesh.text = _menuItemDescriptions[panelY][panelX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.UIGlow;
                }
                else
                {
                    tile.Renderer.TextMesh.text = _menuItemDescriptions[panelY][panelX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
                }
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
            _previouslyHighlightedRowY = _highlightedRowY;
        }

        public static void ProcessInput()
        {
            if (!_Enabled) return;

            var highlight = MouseSystem.GetHighlightedTile();
            if (highlight != null)
            {
                var hPos = highlight.Location;
                var xInRange = hPos.x > MiddelPanel.xMin + 3 && hPos.x < MiddelPanel.xMax - 3;
                var yInRange = hPos.y > MiddelPanel.yMax - _menuItemDescriptions.Count && hPos.y <= MiddelPanel.yMax;
                if (xInRange && yInRange)
                {
                    var inventoryIndex = MiddelPanel.yMax - hPos.y;
                    var validSelection = !String.IsNullOrEmpty(_menuItemDescriptions[inventoryIndex].Trim());
                    if (validSelection)
                    {
                        _highlightedRowY = hPos.y;
                        _highlightedRowIndex = inventoryIndex;

                        // highlight row
                        for (int x = 0; x < MiddelPanel.width; x++)
                        {
                            var t = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, hPos.y);
                            t.Highlighted = true;
                        }
                    }
                    else
                    {
                        _highlightedRowY = -1;
                        _highlightedRowIndex = -1;
                    }
                }
                else
                {
                    _highlightedRowY = -1;
                    _highlightedRowIndex = -1;
                }
            }

            if (Input.GetMouseButtonDown(0) && _highlightedRowIndex >= 0)
            {
                var selection = _menuItemDescriptions[_highlightedRowIndex].Trim();
                switch(selection)
                {
                    case NewGameOption:
                        GameState.ToggleMainMenu();
                        MapSystem.InitializeMap();
                        break;
                    case ContinueOption:
                        GameState.ToggleMainMenu();
                        MapSystem.InitializeMap(true);
                        break;
                }
                GameState.ToggleUI(true);
                GameState.CurrentState = GameState.State.PlayerMoving;
            }
        }
    }
}
