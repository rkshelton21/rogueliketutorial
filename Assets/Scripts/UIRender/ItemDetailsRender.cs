﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class ItemDetailsRender
    {
        static RectInt MiddelPanel = Constants.DetailsDialog;
        static List<string> _descriptions;

        public static void OpenItemDetails(Entity e)
        {
            RegisterTiles();
            UpdateDescription(e);
            UIRender.InventoryOptionsRender.OpenInventoryOptions(e);
        }

        public static void CloseItemDetails(bool returnToInventory)
        {
            DeRegisterTiles();
            _descriptions = null;
            UIRender.InventoryOptionsRender.CloseInventoryOptions();
            if(returnToInventory)
                UIRender.InventoryRender.OpenInventory();
        }

        private static void UpdateDescription(Entity e)
        {
            _descriptions = new List<string>();
            var desc = String.IsNullOrEmpty(e.Description) ? "This item is self explanitory." : e.Description;

            foreach (var msg in MessageSystem.Wrap(desc, Constants.MessageLayout.width - CharacterLibrary.Characters.UISettings.MessageBuffer_Size))
            {
                _descriptions.Add(String.Format("{0}{1}{2}", CharacterLibrary.Characters.UISettings.MessageBuffer_Front, msg.PadRight(MessageRender.MessageLength - CharacterLibrary.Characters.UISettings.MessageBuffer_Size), CharacterLibrary.Characters.UISettings.MessageBuffer_Back));
            }

            RegisterTiles();
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.RenderFunction = RenderItemDetailsTile;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void RenderItemDetailsTile(RLTile tile)
        {
            if (tile.TestData == "Q")
                tile.TestData = "Q";

            var panelY = MiddelPanel.yMin + MiddelPanel.height - tile.Location.y;
            var panelX = tile.Location.x - MiddelPanel.xMin;

            if (panelY < _descriptions.Count)
            {
                {
                    tile.Renderer.TextMesh.text = _descriptions[panelY][panelX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
                }
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
        }
    }
}
