﻿using Assets.Scripts;
using Assets.Scripts.Components;
using Assets.Scripts.UIRender;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRenderer
{
    void Initialize()
    {

    }

    // Update is called once per frame
    public void RenderUI()
    {
        if (ControllerSystem.Player == null) return;

        if (Input.GetKeyDown(KeyCode.I))
        {
            GameState.ToggleInventory();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameState.ToggleDebugger();
        }

        var c = ControllerSystem.Player.Owner.GetComponent<Combatant>(ComponentType.Combatant);
        LeftPanelRender.UpdatePanel(c, c.MaxHP);
        MessageRender.UpdateDisplayQueue();
        //InventoryRender.RenderInventory();

        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            Vector2 touchPos = new Vector2();
            var processInput = false;
            if (Input.touchCount == 0)
            {
                touchPos = MouseSystem.GetHighlightedTile().Location;
                processInput = true;
            }
            else
            {
                Touch myTouch = Input.touches[0];
                if (myTouch.phase == TouchPhase.Ended)
                {
                    processInput = true;
                    //touchPos = myTouch.position;
                    touchPos = MouseSystem.GetHighlightedTile().Location;
                }
            }

            if (processInput)
            {
                var xOutsideNormalRange = touchPos.x < Constants.DialogBuffer || touchPos.x > Constants.FullWidth - Constants.DialogBuffer - 1;
                var yOutsideNormalRange = touchPos.y < Constants.DialogBuffer + 1 || touchPos.y > Constants.FullHeight - Constants.DialogBuffer;

                var xOutsideMap = touchPos.x < Constants.MapRender.xMin || touchPos.x > Constants.MapRender.xMax;
                var yOutsideMap = touchPos.y < Constants.MapRender.yMin || touchPos.y > Constants.MapRender.yMax;

                if (GameState.CurrentState == GameState.State.ViewingInventory || GameState.CurrentState == GameState.State.ViewingItem)
                {
                    if (xOutsideNormalRange || yOutsideNormalRange)
                    {
                        GameState.ToggleInventory();
                    }
                }
                else if (GameState.CurrentState == GameState.State.Targeting)
                {
                    if (xOutsideMap || yOutsideMap)
                    {
                        GameState.ToggleInventory();
                    }
                }
            }
        }
    }

    public static void ReserveTiles(MapInfo info)
    {
        LeftPanelRender.ReserveAllTiles(info);
        MessageRender.ReserveAllTiles(info);
    }

    public void ReStart()
    {
    }

    public static void TargetTile(RLTile tile, int range)
    {
        tile.Add(new RenderModifier()
        {
            Display = new DisplayProperties()
            {
                Representation = CharacterLibrary.Characters.UISettings.Targeting,
                Color = ColorLibrary.Colors.UIColor,
                Background = ColorLibrary.Colors.UIGlow
            },
            ModifyBackground = true,
            ModifyColor = true,
            ModifyText = !tile.Occupied,
            RenderCounter = 1
        });

        if (range > 2)
            Debug.LogError("Not yet supported.");

        if (range > 1)
        {
            var neighbors = MappingUtilities.Neighbors(tile);
            foreach (var t in neighbors)
            {
                if (tile.ReservedForUI) continue;
                t.Add(new RenderModifier()
                {
                    Display = new DisplayProperties()
                    {
                        Representation = CharacterLibrary.Characters.UISettings.Targeting,
                        Color = ColorLibrary.Colors.UIColor,
                        Background = ColorLibrary.Colors.UIGlow
                    },
                    ModifyBackground = true,
                    ModifyColor = true,
                    ModifyText = true,
                    RenderCounter = 1
                });
            }
        }
    }
}
