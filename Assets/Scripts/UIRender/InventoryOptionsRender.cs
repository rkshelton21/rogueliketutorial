﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class InventoryOptionsRender
    {
        static RectInt MiddelPanel = Constants.DetailsOptions;
        static int _highlightedColumnIndex = -1;
        static int _previouslyHighlightedColumn = -1;
        static bool _inventoryOptionsOpen = false;
        static List<string> _options;
        static Entity _selectedItem = null;

        public static void OpenInventoryOptions(Entity selectedItem)
        {
            _inventoryOptionsOpen = true;
            _selectedItem = selectedItem;
            UpdateOptions();
        }

        public static void CloseInventoryOptions()
        {
            _inventoryOptionsOpen = false;
            DeRegisterTiles();
            _options = null;
            _selectedItem = null;
        }

        private static void UpdateOptions()
        {
            _options = new List<string>();
            var optionLength = MiddelPanel.width / 3;
            var options = new string[] { "Use", "Drop", "Throw" };

            var desc0 = options[0].PadLeft((optionLength - options[0].Length) / 2 + options[0].Length, ' ').PadRight(optionLength, ' ');
            var desc1 = options[1].PadLeft((optionLength - options[1].Length) / 2 + options[1].Length, ' ').PadRight(optionLength, ' ');
            var desc2 = options[2].PadLeft((optionLength - options[2].Length) / 2 + options[2].Length, ' ').PadRight(optionLength, ' ');
            _options.Add(desc0);
            _options.Add(desc1);
            _options.Add(desc2);

            RegisterTiles();
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.RenderFunction = RenderOptionTile;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < MiddelPanel.width; x++)
            {
                for (int y = 0; y < MiddelPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, MiddelPanel.yMin + MiddelPanel.height - y);
                    tile.Highlighted = false;
                    tile.RenderFunction = null;
                }
            }
        }

        public static void ProcessInput()
        {
            if (!_inventoryOptionsOpen) return;

            Process();
        }

        private static void Process()
        {
            _highlightedColumnIndex = -1;
            _previouslyHighlightedColumn = -1;

            var highlight = MouseSystem.GetHighlightedTile();
            if (highlight != null)
            {
                var hPos = highlight.Location;
                var xInRange = hPos.x >= MiddelPanel.xMin && hPos.x <= MiddelPanel.xMax;
                var yInRange = 1 == MiddelPanel.yMin + MiddelPanel.height - hPos.y;

                if (xInRange && yInRange)
                {
                    var panelX = hPos.x - MiddelPanel.xMin;
                    var optionLength = MiddelPanel.width / 3;
                    _highlightedColumnIndex = Mathf.FloorToInt(panelX / optionLength);

                    var start = optionLength * _highlightedColumnIndex;
                    var finish = Mathf.Min(optionLength * (_highlightedColumnIndex + 1), MiddelPanel.width);
                    // highlight row
                    for (int x = start; x < finish; x++)
                    {
                        var t = MapSystem.CurrentLevelTile(MiddelPanel.xMin + x, hPos.y);
                        t.Highlighted = true;
                    }
                }
            }

            if (Input.GetMouseButtonDown(0) && _highlightedColumnIndex >= 0 && _highlightedColumnIndex < 3)
            {
                switch(_highlightedColumnIndex)
                {
                    case 0:
                        UseItem();
                        break;
                    case 1:
                        DropItem();
                        break;
                    case 2:
                        ThrowItem();
                        break;
                }
                //Debug.Log("Clicked option " + _highlightedColumnIndex);
            }
        }

        public static void RenderOptionTile(RLTile tile)
        {
            if (tile.TestData == "Q")
                tile.TestData = "Q";

            var panelY = MiddelPanel.yMin + MiddelPanel.height - tile.Location.y;
            var panelX = tile.Location.x - MiddelPanel.xMin;
            var optionLength = MiddelPanel.width / 3;
            var currentIndex = Mathf.FloorToInt(panelX / optionLength);
            var wordX = panelX - currentIndex * optionLength;

            if (currentIndex == _previouslyHighlightedColumn)
                tile.NeedsUpdate = true;

            if (panelY == 1 && currentIndex < 3)
            {

                if (currentIndex == _highlightedColumnIndex)
                {
                    tile.Renderer.TextMesh.text = _options[currentIndex][wordX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.UIGlow;
                }
                else
                {
                    tile.Renderer.TextMesh.text = _options[currentIndex][wordX].ToString();
                    tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                    tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
                }
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.DialogBackground;
            }
            _previouslyHighlightedColumn = _highlightedColumnIndex;
        }

        private static void UseItem()
        {
            var modifier = _selectedItem.GetComponent<AttributeModifier>(ComponentType.AttributeModifier);
            var item = _selectedItem.GetComponent<ItemComponent>(ComponentType.Item);

            if (modifier.RequiresTargeting)
            {
                GameState.ToggleTargeting(_selectedItem);
            }
            else
            {
                var used = modifier.ApplyModificationToTarget(modifier, ControllerSystem.Player.Owner, ControllerSystem.Player.Owner);
                if (used)
                {
                    GameState.ToggleInventory();
                }
            }

            if (modifier.Used)
            {
                ControllerSystem.Player.Owner.GetComponent<InventoryComponent>(ComponentType.Inventory).Items.Remove(item);
            }
        }

        private static void ThrowItem()
        {
            Debug.LogError("Throwing?");
        }

        private static void DropItem()
        {
            var playerPos = ControllerSystem.Player.Owner.GridPosition;
            _selectedItem.GridPosition = playerPos;
            MapSystem.CurrentLevelTile(playerPos.x, playerPos.y).Add(_selectedItem);
            var item = _selectedItem.GetComponent<ItemComponent>(ComponentType.Item);
            ControllerSystem.Player.Owner.GetComponent<InventoryComponent>(ComponentType.Inventory).Items.Remove(item);
            MessageSystem.AddMessage(String.Format("You drop the {0}", _selectedItem.Name), ColorLibrary.Colors.Message_Info);
            GameState.ToggleItemDetails(_selectedItem);
        }
    }
}
