﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.UIRender
{
    public static class MessageRender
    {
        private static RectInt TopPanel = Constants.MessageLayout;
        private static List<MessageSystem.MessageInfo> _DisplayQueue = new List<MessageSystem.MessageInfo>();
        public static int MessageLength = 60;
        private static bool _update = false;

        public static void ReserveAllTiles(MapInfo info)
        {
            for (int l = 0; l < info.Levels.Count; l++)
            {
                for (int x = 0; x < TopPanel.width; x++)
                {
                    for (int y = 0; y < TopPanel.height; y++)
                    {
                        var tile = info.Levels[l].Tiles[TopPanel.xMin + x][TopPanel.yMin + TopPanel.height - 1 - y];
                        tile.ReservedForUI = true;
                    }
                }
            }
        }

        public static void RegisterTiles()
        {
            for (int x = 0; x < TopPanel.width; x++)
            {
                for (int y = 0; y < TopPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(TopPanel.xMin + x, TopPanel.yMin + TopPanel.height - 1 - y);
                    tile.RenderFunction = RenderTile;
                    tile.DefaultRenderFunction = tile.RenderFunction;
                }
            }
        }

        public static void DeRegisterTiles()
        {
            for (int x = 0; x < TopPanel.width; x++)
            {
                for (int y = 0; y < TopPanel.height; y++)
                {
                    var tile = MapSystem.CurrentLevelTile(x, y);
                    tile.RenderFunction = null;
                }
            }
        }

        public static void UpdateDisplayQueue()
        {
            var messageQueue = MessageSystem.GetMessages();
            _update = messageQueue.Count > 0;

            while (messageQueue.Count > 0)
            {
                _DisplayQueue.Insert(0, messageQueue.First());
                if (_DisplayQueue.Count > TopPanel.height)
                    _DisplayQueue.RemoveAt(_DisplayQueue.Count - 1);
                MessageSystem.RemoveMessage(0);
            }
        }

        public static void RenderTile(RLTile tile)
        {
            var panelY = TopPanel.yMin + tile.Location.y;
            var panelX = tile.Location.x - TopPanel.xMin;

            if (panelY < _DisplayQueue.Count)
            {
                tile.Renderer.TextMesh.text = _DisplayQueue[panelY].Message[panelX].ToString();
                tile.Renderer.TextMesh.color = _DisplayQueue[panelY].MessageColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }
            else
            {
                tile.Renderer.TextMesh.text = " ";
                tile.Renderer.TextMesh.color = ColorLibrary.Colors.UIColor;
                tile.Renderer.Background.color = ColorLibrary.Colors.UIBaseDark;
            }

            tile.NeedsUpdate = _update;
        }
    }
}
