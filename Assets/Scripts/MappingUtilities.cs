﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public static class MappingUtilities
    {
        internal static void PurgeEntitiesFromTilesAndMarkForUpdate(this LevelData level)
        {
            foreach (var t in level.Tiles)
            {
                foreach (var tt in t)
                {
                    tt.UpdateEntities();
                }
            }
        }

        internal static void MarkAllTilesForUpdate(this LevelData level)
        {
            for (int x = 0; x < Constants.MapWidth; x++)
            {
                for (int y = 0; y < Constants.MapHeight; y++)
                {
                    level.Tiles[x][y].NeedsUpdate = true;
                }
            }
        }

        internal static int IndexFor(int x, int y)
        {
            return x + y * Constants.MapWidth;
        }

        internal static int IndexFor(RLTile cell)
        {
            return IndexFor(cell.Location.x, cell.Location.y);
        }

        /// <summary>
        /// Get an IEnumerable of all Cells in the Map
        /// </summary>
        /// <returns>IEnumerable of all Cells in the Map</returns>
        public static IEnumerable<RLTile> GetAllCells(this LevelData level)
        {
            for (int y = 0; y < Constants.MapHeight; y++)
            {
                for (int x = 0; x < Constants.MapWidth; x++)
                {
                    yield return level.GetCell(x, y);
                }
            }
        }

        /// <summary>
        /// Get an IEnumerable of Cells in a square area around the center Cell up to the specified distance
        /// </summary>
        /// <param name="xCenter">X location of the center Cell with 0 as the farthest left</param>
        /// <param name="yCenter">Y location of the center Cell with 0 as the top</param>
        /// <param name="distance">The number of Cells to get in each direction from the center Cell</param>
        /// <returns>IEnumerable of Cells in a square area around the center Cell</returns>
        public static IEnumerable<RLTile> GetCellsInSquare(this LevelData level, int xCenter, int yCenter, int distance)
        {
            int xMin = Math.Max(0, xCenter - distance);
            int xMax = Math.Min(Constants.MapWidth - 1, xCenter + distance);
            int yMin = Math.Max(0, yCenter - distance);
            int yMax = Math.Min(Constants.MapHeight - 1, yCenter + distance);

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    yield return level.GetCell(x, y);
                }
            }
        }

        /// <summary>
        /// Get an IEnumerable of Cells in a line from the Origin Cell to the Destination Cell
        /// The resulting IEnumerable includes the Origin and Destination Cells
        /// Uses Bresenham's line algorithm to determine which Cells are in the closest approximation to a straight line between the two Cells
        /// </summary>
        /// <param name="xOrigin">X location of the Origin Cell at the start of the line with 0 as the farthest left</param>
        /// <param name="yOrigin">Y location of the Origin Cell at the start of the line with 0 as the top</param>
        /// <param name="xDestination">X location of the Destination Cell at the end of the line with 0 as the farthest left</param>
        /// <param name="yDestination">Y location of the Destination Cell at the end of the line with 0 as the top</param>
        /// <returns>IEnumerable of Cells in a line from the Origin Cell to the Destination Cell which includes the Origin and Destination Cells</returns>
        public static IEnumerable<RLTile> GetCellsAlongLine(this LevelData level, int xOrigin, int yOrigin, int xDestination, int yDestination)
        {
            xOrigin = ClampX(xOrigin);
            yOrigin = ClampY(yOrigin);
            xDestination = ClampX(xDestination);
            yDestination = ClampY(yDestination);

            int dx = Math.Abs(xDestination - xOrigin);
            int dy = Math.Abs(yDestination - yOrigin);

            int sx = xOrigin < xDestination ? 1 : -1;
            int sy = yOrigin < yDestination ? 1 : -1;
            int err = dx - dy;

            while (true)
            {
                yield return level.GetCell(xOrigin, yOrigin);
                if (xOrigin == xDestination && yOrigin == yDestination)
                {
                    break;
                }
                int e2 = 2 * err;
                if (e2 > -dy)
                {
                    err = err - dy;
                    xOrigin = xOrigin + sx;
                }
                if (e2 < dx)
                {
                    err = err + dx;
                    yOrigin = yOrigin + sy;
                }
            }
        }

        /// <summary>
        /// Get an IEnumerable of outermost border Cells in a square area around the center Cell up to the specified distance
        /// </summary>
        /// <param name="xCenter">X location of the center Cell with 0 as the farthest left</param>
        /// <param name="yCenter">Y location of the center Cell with 0 as the top</param>
        /// <param name="distance">The number of Cells to get in each direction from the center Cell</param>
        /// <returns>IEnumerable of outermost border Cells in a square area around the center Cell</returns>
        public static IEnumerable<RLTile> GetBorderCellsInSquare(this LevelData level, int xCenter, int yCenter, int distance)
        {
            int xMin = Math.Max(0, xCenter - distance);
            int xMax = Math.Min(Constants.MapWidth - 1, xCenter + distance);
            int yMin = Math.Max(0, yCenter - distance);
            int yMax = Math.Min(Constants.MapHeight - 1, yCenter + distance);

            for (int x = xMin; x <= xMax; x++)
            {
                yield return level.GetCell(x, yMin);
                yield return level.GetCell(x, yMax);
            }
            for (int y = yMin + 1; y <= yMax - 1; y++)
            {
                yield return level.GetCell(xMin, y);
                yield return level.GetCell(xMax, y);
            }
        }

        private static int ClampX(int x)
        {
            return (x < 0) ? 0 : (x > Constants.MapWidth - 1) ? Constants.MapWidth - 1 : x;
        }

        private static int ClampY(int y)
        {
            return (y < 0) ? 0 : (y > Constants.MapHeight - 1) ? Constants.MapHeight - 1 : y;
        }

        private static RLTile GetCell(this LevelData level, int x, int y)
        {
            return level.Tiles[x][y];
        }

        public static List<RLTile> Neighbors(RLTile t)
        {
            var ret = new List<RLTile>();
            var nPos = t.Location + new Vector2Int(0, 1);
            var sPos = t.Location + new Vector2Int(0, -1);
            var ePos = t.Location + new Vector2Int(1, 0);
            var wPos = t.Location + new Vector2Int(-1, 0);

            var n = MapSystem.CurrentLevelTile(nPos.x, nPos.y);
            var s = MapSystem.CurrentLevelTile(sPos.x, sPos.y);
            var e = MapSystem.CurrentLevelTile(ePos.x, ePos.y);
            var w = MapSystem.CurrentLevelTile(wPos.x, wPos.y);

            if (n != null) ret.Add(n);
            if (s != null) ret.Add(s);
            if (e != null) ret.Add(e);
            if (w != null) ret.Add(w);

            return ret;
        }

        public static List<RLTile> GetCellsInRange(Vector2Int center, int radius)
        {
            var tilesInRange = new List<RLTile>();
            for (int x = center.x-radius; x <= center.x + radius; x++)
            {
                for (int y = center.y - radius; y <= center.y + radius; y++)
                {
                    var pos = new Vector2Int(x, y);
                    var inRange = (pos - center).magnitude <= radius;
                    if (inRange)
                    {
                        var t = MapSystem.CurrentLevelTile(pos.x, pos.y);
                        if (t != null)
                        {
                            tilesInRange.Add(t);
                        }
                    }
                }
            }

            return tilesInRange;
        }
    }
}
