﻿using Assets.Scripts.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public static class XPUtilities
    {
        public static void SetXP(Entity e)
        {
            var c = e.GetComponent<Combatant>(Components.ComponentType.Combatant);
            c.XP = e.Depth;
        }

        public static void AddXP(Combatant c, int xp)
        {
            c.XP += xp;
            while(c.XP > c.Level * Constants.XPScale)
            {
                c.XP -= (c.Level * Constants.XPScale);
                c.Level++;
            }
        }
    }
}
