﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    [Serializable]
    public class CharacterLibrary
    {
        [Serializable]
        public class Entities
        {
            public Entities()
            {
                DeadBody = '%';
                Player = '@';
                Potion = 'p';
                Weapon = 'W';
                Scroll = '?';
                Orc = 'o';
                Troll = 'T';
            }

            public char DeadBody { get; set; }
            public char Player { get; set; }
            public char Potion { get; set; }
            public char Weapon { get; set; }
            public char Scroll { get; set; }
            public char Orc { get; set; }
            public char Troll { get; set; }
        }

        [Serializable]
        public class Environment
        {
            public Environment()
            {
                NotSet = 'X';
                Ground = '-';
                Wall = '#';
                Stairs_Up = '^';
                Stairs_Down = 'v';
            }

            public char NotSet { get; set; }
            public char Ground { get; set; }
            public char Wall { get; set; }
            public char Stairs_Up { get; set; }
            public char Stairs_Down { get; set; }
        }

        [Serializable]
        public class UI
        {
            public UI()
            {
                MessageBuffer_Front = "  ";
                MessageBuffer_Back = "  ";
                MessageBuffer_Size = 4;

                InventoryBuffer_Front = "   ";
                InventoryBuffer_Back = "   ";
                InventoryBuffer_Size = 4;

                LeftPanelBackground = ' ';
                LeftPanelHealth = '>';
                LeftPanelHealthMissing = ' ';

                Targeting = (char)(1421);
            }

            public String MessageBuffer_Front { get; set; }
            public String MessageBuffer_Back { get; set; }
            public int MessageBuffer_Size { get; set; }

            public String InventoryBuffer_Front { get; set; }
            public String InventoryBuffer_Back { get; set; }
            public int InventoryBuffer_Size { get; set; }

            public char LeftPanelBackground { get; set; }
            public char LeftPanelHealth { get; set; }
            public char LeftPanelHealthMissing { get; set; }

            public char Targeting { get; set; }

        }

        private static CharacterLibrary _Characters = null;

        public static CharacterLibrary Characters
        {
            get
            {
                if (_Characters == null)
                {
                    _Characters = DataStorage.LoadCharacterLibrary();
                    if (_Characters == null)
                        _Characters = new CharacterLibrary();
                }
                return _Characters;
            }
            set
            {
                _Characters = value;
            }
        }

        public UI UISettings { get; set; }
        public Entities EntitySettings { get; set; }
        public Environment EnvironmentSettings { get; set; }

        public CharacterLibrary()
        {
            UISettings = new UI();
            EntitySettings = new Entities();
            EnvironmentSettings = new Environment();
        }
    }
}
