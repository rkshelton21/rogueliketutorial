﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSystem : MonoBehaviour
{
    public bool DebugRender;
    private static MapSystem _instance;

    private MapInfo _map;
    private MapFOV _fov;
    private System.Random _rand = new System.Random(1);
    private Vector2Int _previousPlayerPosition;
    private bool _debugRender = false;

    private static void Init(MapSystem instance)
    {
        if (_instance == null)
        {
            _instance = instance;
        }
    }

    private void InitMap(bool load = false)
    {
        if (load)
        {
            _map = MapInfo.LoadMap();
        }
        else
        {
            _map = MapInfo.CreateNewMap();
        }
        //GameState.ToggleUI(true);
        PathingSystem.ReStart();
        _fov = new MapFOV(_map.Levels[_map.CurrentDepth]);
    }

    void Awake()
    {
        _debugRender = DebugRender;
        Init(this);
    }

    void Start()
    {
        // Start on the menu
        InitializeEmptyMap();
    }

    // Update is called once per frame
    public void Update()
    {
        if (_map == null || _map.CurrentTiles == null)
            return;

        // if debug render is toggled, update all tiles
        if (_debugRender != DebugRender)
        {
            _debugRender = DebugRender;
            _map.CurrentLevelData.MarkAllTilesForUpdate();
        }

        UpdateFOV();

        _map.CurrentLevelData.PurgeEntitiesFromTilesAndMarkForUpdate();
    }

    public static void AddToMap(Entity c)
    {
        var pos = c.GridPosition;
        _instance._map.Tile(new Vector2Int(pos.x,pos.y), c.Depth).Add(c);
    }

    public static RLTile CurrentLevelTile(int x, int y)
    {
        if (_instance == null || _instance._map == null || _instance._map.CurrentTiles == null)
            return null;

        if (x >= Constants.MapWidth || x < 0)
            return null;
        if (y >= Constants.MapHeight || y < 0)
            return null;

        return _instance._map.CurrentTiles[x][y];
    }

    public static RLTile Tile(int x, int y, int depth)
    {
        if (_instance == null || _instance._map == null || _instance._map.CurrentTiles == null)
            return null;

        if (x >= Constants.MapWidth || x < 0)
            return null;
        if (y >= Constants.MapHeight || y < 0)
            return null;

        return _instance._map.Levels[depth].Tiles[x][y];
    }

    public static RLTile RandomWalkableTile(int depth)
    {
        var x = 0;
        var y = 0;
        var index = _instance._rand.Next(0, Constants.MapWidth * Constants.MapHeight);
        var found = false;
        while (!found)
        {
            x = index % Constants.MapWidth;
            y = (int)((index - x) / Constants.MapWidth);
            found = Tile(x, y, depth).Walkable;

            index++;
            if (index >= Constants.MapWidth * Constants.MapHeight)
                index = 0;
        }

        return Tile(x, y, depth);
    }

    public static void InitializeMap(bool load = false)
    {
        _instance.InitMap(load);
        RemapRenderersToNewTiles();
    }

    public static void InitializeEmptyMap()
    {
        _instance._map = MapInfo.CreateEmptyMap();
        RemapRenderersToNewTiles();
        Assets.Scripts.UIRender.MainMenuRender.RegisterTiles();
    }

    private static void RemapRenderersToNewTiles()
    {
        for (int x = 0; x < Constants.MapWidth; x++)
        {
            for (int y = 0; y < Constants.MapHeight; y++)
            {
                var tile = CurrentLevelTile(x, y);
                var matchingTile = GameObject.Find(string.Format("Tile: {0}, {1}", x, y));
                var renderer = matchingTile.GetComponent<TileRenderer>();
                tile.Renderer = renderer;
                tile.NeedsUpdate = true;
            }
        }
    }

    public static void UpdateFOV()
    {
        if (_instance._map == null || _instance._map.Player == null)
            return;

        var playerPos = _instance._map.Player.GridPosition;
        if (playerPos != _instance._previousPlayerPosition)
        {
            _instance._previousPlayerPosition = playerPos;
            _instance._fov.ComputeFov(playerPos.x, playerPos.y, Constants.PlayerViewDistance, true, MapSystem.CurrentDepth);
            _instance._fov.UpdateFOVRender();
        }
    }

    public static LevelData CurrentLevel
    {
        get { return _instance._map.Levels[CurrentDepth]; }
    }

    public static int CurrentDepth
    {
        get { return _instance._map.CurrentDepth; }
    }

    public static List<LevelData> Levels
    {
        get { return _instance._map.Levels;  }
    }

    public static bool DebugRendering()
    {
        return _instance.DebugRender;
    }

    public static void NextLevel()
    {
        _instance._map.CurrentDepth++;
        _instance._map.Player.GridPosition = MapSystem.CurrentLevel.StairsUp;
        _instance._map.Player.Depth = _instance._map.CurrentDepth;
        var combatant = _instance._map.Player.GetComponent<Combatant>(ComponentType.Combatant);
        if (combatant.Level < _instance._map.CurrentDepth)
        {
            MessageSystem.AddMessage("You feel stronger.", ColorLibrary.Colors.Message_StatBuff);
            //combatant.Level = _instance._map.CurrentDepth;
            combatant.HP = Mathf.Min(combatant.MaxHP, combatant.HP + combatant.MaxHP / 2);
        }
        AddToMap(_instance._map.Player);
        _instance._fov = new MapFOV(_instance._map.Levels[_instance._map.CurrentDepth]);

        RemapRenderersToNewTiles();
        GameState.ToggleUI(true);

        _instance._previousPlayerPosition = new Vector2Int();
    }

    public static void PreviousLevel()
    {
        _instance._map.CurrentDepth--;
        _instance._map.Player.GridPosition = MapSystem.CurrentLevel.StairsDown;
        _instance._map.Player.Depth = _instance._map.CurrentDepth;
        AddToMap(_instance._map.Player);
        _instance._fov = new MapFOV(_instance._map.Levels[_instance._map.CurrentDepth]);

        RemapRenderersToNewTiles();
        GameState.ToggleUI(true);

        _instance._previousPlayerPosition = new Vector2Int();
    }
}
