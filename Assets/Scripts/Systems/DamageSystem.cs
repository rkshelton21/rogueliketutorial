﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSystem : MonoBehaviour
{
    private static DamageSystem _instance;

    private static void Init(DamageSystem instance)
    {
        if (_instance == null) _instance = instance;
    }

    void Awake()
    {
        Init(this);
    }

    public static void PerformAttack(Entity attacker, Entity defender, bool seen)
    {
        var attackerC = attacker.GetComponent<Combatant>(ComponentType.Combatant);
        var defenderC = defender.GetComponent<Combatant>(ComponentType.Combatant);

        var damage = Mathf.Max(0, (attackerC.Damage + attackerC.DamageBonus) - (defenderC.Defense + defenderC.DefenseBonus));
        defenderC.HP -= damage;

        if (defender.ID == ControllerSystem.Player.Owner.ID)
        {
            ApplyDamageToPlayer(damage, attacker, attackerC, defender, defenderC);
        }
        else
        {
            ApplyDamageToEntity(damage, attacker, attackerC, defender, defenderC, seen);
        }
    }

    public static void ApplyDamage(Entity attacker, Entity defender, float damage, bool seen)
    {
        var attackerC = attacker.GetComponent<Combatant>(ComponentType.Combatant);
        var defenderC = defender.GetComponent<Combatant>(ComponentType.Combatant);

        defenderC.HP -= damage;

        if (defender.ID == ControllerSystem.Player.Owner.ID)
        {
            ApplyDamageToPlayer(damage, attacker, attackerC, defender, defenderC);
        }
        else
        {
            ApplyDamageToEntity(damage, attacker, attackerC, defender, defenderC, seen);
        }
    }

    public static void ApplyDamageToEntity(float damage, Entity attacker, Combatant attackerC, Entity defender, Combatant defenderC, bool seen)
    {
        if (damage == 0)
        {
            if(seen) MessageSystem.AddMessage(string.Format("The {0} laughs in your face.", defender.Name), ColorLibrary.Colors.Message_Warning);
            return;
        }

        if (defenderC.HP > 0)
        {
            if(seen) MessageSystem.AddMessage(string.Format("The {0} takes {1} damage.", defender.Name, damage), ColorLibrary.Colors.Message_Warning);
        }
        else
        {
            var e = defenderC.Owner;
            e.Active = false;
            e.RenderOrder = 100;
            e.Display.Representation = CharacterLibrary.Characters.EntitySettings.DeadBody;
            e.Display.Color = ColorLibrary.Colors.DeadBody;
            if(seen) MessageSystem.AddMessage(string.Format("The {0} is slain.", defender.Name), ColorLibrary.Colors.Message_Warning);
            MapSystem.Tile(e.GridPosition.x, e.GridPosition.y, e.Depth).NeedsUpdate = true;

            if (attacker.ID == ControllerSystem.Player.Owner.ID)
            {
                XPUtilities.AddXP(attackerC, defenderC.XP);
            }
        }
    }

    private static void ApplyDamageToPlayer(float damage, Entity attacker, Combatant attackerC, Entity defender, Combatant defenderC)
    {
        if (damage == 0)
        {
            MessageSystem.AddMessage(string.Format("You laugh in the face of the {0}.", attacker.Name), ColorLibrary.Colors.Message_Damage);
            return;
        }

        if (defenderC.HP > 0)
        {
            // kill
            MessageSystem.AddMessage(string.Format("You take {1} damage from the {0}.", attacker.Name, damage), ColorLibrary.Colors.Message_Damage);
        }
        else
        {
            defenderC.Owner.Active = false;
            MessageSystem.AddMessage(string.Format("You are slain by the {0}.", attacker.Name), ColorLibrary.Colors.Message_Damage);
            MapSystem.Tile(defenderC.Owner.GridPosition.x, defenderC.Owner.GridPosition.y, defenderC.Owner.Depth).NeedsUpdate = true;
            GameState.GameOver();
        }
    }
}
