﻿using Assets.Scripts;
using Assets.Scripts.UIRender;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MessageSystem : MonoBehaviour {

    [Serializable]
    public struct MessageInfo
    {
        public String Message;
        public Color MessageColor;
    }

    private static List<MessageInfo> _MessageQueue = new List<MessageInfo>();

    // Use this for initialization
    void Start()
    {

    }

	// Update is called once per frame
	void LateUpdate () {
        //var n = _MessageQueue.Count;

        //for(int i=0; i<n; i++)
        //{
        //    Debug.LogWarning(_MessageQueue[i]);
        //}
        //_MessageQueue.RemoveRange(0, n);
    }

    public static void AddMessage(String message)
    {
        AddMessage(message, ColorLibrary.Colors.Message_Info);
    }

    public static void AddMessage(string message, Color color)
    {
        foreach (var msg in Wrap(message, Constants.MessageLayout.width - CharacterLibrary.Characters.UISettings.MessageBuffer_Size))
        {
            var formattedMessage = new MessageInfo()
            {
                Message = String.Format("{0}{1}{2}", CharacterLibrary.Characters.UISettings.MessageBuffer_Front, msg.PadRight(MessageRender.MessageLength - CharacterLibrary.Characters.UISettings.MessageBuffer_Size), CharacterLibrary.Characters.UISettings.MessageBuffer_Back),
                MessageColor = color
            };
            _MessageQueue.Add(formattedMessage);
        }
    }

    public static List<MessageInfo> GetMessages() { return _MessageQueue; }

    public static void RemoveMessage(int index)
    {
        _MessageQueue.RemoveAt(index);
    }

    public static void SetMessage(List<MessageInfo> messages)
    {
        _MessageQueue = messages;
    }

    public static void ClearMessages()
    {
        _MessageQueue.Clear();
    }

    public static string[] Wrap(string text, int max)
    {
        var charCount = 0;
        var lines = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        return lines.GroupBy(w => (charCount += (((charCount % max) + w.Length + 1 >= max)
                        ? max - (charCount % max) : 0) + w.Length + 1) / max)
                    .Select(g => string.Join(" ", g.ToArray()))
                    .ToArray();
    }
}
