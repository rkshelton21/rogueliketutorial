﻿using Assets.Scripts;
using Assets.Scripts.Components;
using Assets.Scripts.UIRender;
using RogueSharp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSystem : MonoBehaviour {

    public bool ShowPathing = false;
    private static MouseSystem _instance;

    private Vector2 _click1;
    private Vector2 _click2;
    private int clickCounter = 0;
    private RLTile _start;
    private RLTile _finish;
    private Path _path;
    private RLTile _hoverTile;

    public static RLTile GetHighlightedTile()
    {
        return _instance._hoverTile;
    }

    private static void Init(MouseSystem instance)
    {
        if (_instance == null) _instance = instance;
    }

    void Awake()
    {
        Init(this);
    }

    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	void Update () {
        if (_hoverTile != null)
        {
            _hoverTile.Hovered = false;
            _hoverTile.TestData = "";
        }

        _hoverTile = GetTile(Input.mousePosition);
        //do them in this order so they don't accidentally cascade input
        LeftPanelRender.ProcessInput();
        TargetingRender.ProcessInput();
        InventoryOptionsRender.ProcessInput();
        InventoryRender.ProcessInput();
        MainMenuRender.ProcessInput();

        if (_hoverTile != null && _hoverTile.InFOV)
        {
            _hoverTile.Hovered = true;
            if (GameState.CurrentState == GameState.State.Targeting)
            {
                //_hoverTile.SetNextRenderFunction(UIRenderer.RenderTargetingTile);
                UIRenderer.TargetTile(_hoverTile, TargetingRender.GetVisibleTargetingRange());
                _hoverTile.TestData = "Q";
            }
        }
        else
        {
            return;
        }

        if (ShowPathing && Input.GetMouseButtonDown(0))
        {

            if (_hoverTile.Occupied)
            {
                var occupant = _hoverTile.GetFirstEntity();
                var controller = occupant.GetComponent<ControllerComponent>(Assets.Scripts.Components.ComponentType.Controller);
                var path = controller.GetPath();
                HighlightPath(path);
                return;
            }

            switch (clickCounter)
            {
                case 0:
                    _start = _hoverTile;
                    clickCounter++;
                    break;
                case 1:
                    _finish = _hoverTile;
                    clickCounter = 0;
                    HighlightPath();
                    break;
                default:
                    break;
            }
        }
    }

    private Vector2Int GetPosition(Vector3 mousePosition)
    {
        //if(GameState.CurrentState == GameState.State.Targeting)
            //return new Vector2Int(30, 10);
        Camera c = Camera.main;
        var p = c.ScreenToWorldPoint(mousePosition);
        return new Vector2Int((int)(p.x + 0.5f), (int)(p.y + 0.5f));
    }

    private RLTile GetTile(Vector3 mousePosition)
    {
        var p = GetPosition(mousePosition);
        if(GameState.CurrentState == GameState.State.Targeting)
            return DisplayMapRender.GetRenderMapTile(p);
        else
            return MapSystem.CurrentLevelTile(p.x, p.y);

    }

    private void HighlightPath(Path newPath = null)
    {
        if (_path != null)
        {
            foreach (var p in _path.Steps)
            {
                p.Highlighted = false;
            }
        }

        if (newPath == null)
        {
            if (_start == null || _finish == null)
                return;

            _path = PathingSystem.GetPath(_start, _finish, _finish.Depth);
        }
        else
        {
            _path = newPath;
        }

        foreach (var p in _path.Steps)
        {
            p.Highlighted = true;
        }
    }
}
