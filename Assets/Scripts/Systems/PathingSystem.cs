﻿using RogueSharp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathingSystem : MonoBehaviour {

    private static PathingSystem _instance;
    private List<PathFinder> _pathing = new List<PathFinder>();

    private static void Init(PathingSystem instance)
    {
        if (_instance == null) _instance = instance;
    }

    void Awake()
    {
        Init(this);
    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static Path GetPath(RLTile start, RLTile finish, int depth)
    {
        return _instance._pathing[depth].ShortestPath(start, finish, depth);
    }

    // Use this for initialization
    public static void ReStart()
    {
        _instance._pathing.Clear();
        foreach (var level in MapSystem.Levels)
        {
            _instance._pathing.Add(new PathFinder(level, 1.41d));
        }
    }
}
