﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class BehaviorSystem : MonoBehaviour {

    private static BehaviorSystem _instance;
    
    private static void Init(BehaviorSystem instance)
    {
        if (_instance == null) _instance = instance;
    }

    void Awake()
    {
        Init(this);
    }

    public static AIBehavior GetBehavior(Entity entity, AIBehavior currentBehavior)
    {
        var playerDepth = ControllerSystem.Player.Owner.Depth;
        if (playerDepth != entity.Depth)
            return AIBehavior.Wandering;

        var playerPos = ControllerSystem.Player.Owner.GridPosition;
        var dx = playerPos.x - entity.GridPosition.x;
        var dy = playerPos.y - entity.GridPosition.y;
        var distance = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));

        if (distance < 5f)
        {
            return AIBehavior.Hunting;
        }

        return AIBehavior.Wandering;
    }
}
