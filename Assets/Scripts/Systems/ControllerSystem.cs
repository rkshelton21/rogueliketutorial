﻿using Assets.Scripts;
using Assets.Scripts.Components;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSystem : MonoBehaviour {

    private static ControllerSystem _instance;
    public static ControllerComponent Player;
    public const float MovementDelay = 0.15f;
    public float _cooldown = 0f;

    private List<ControllerComponent> _controllers = new List<ControllerComponent>();

    private static void Init(ControllerSystem instance)
    {
        if (_instance == null) _instance = instance;
    }

    void Awake()
    {
        Init(this);
    }

	// Update is called once per frame
	void Update () {
        _cooldown -= Time.deltaTime;

        if (GameState.CurrentState == GameState.State.PlayerMoving)
        {
            if (_cooldown < 0f && Player != null)
            {
                var moveStatus = Player.Move(new Vector2Int());
                if (moveStatus == MovementRules.MovementStatus.Occupied || moveStatus == MovementRules.MovementStatus.Valid)
                {
                    _cooldown = MovementDelay;
                    if (GameState.CurrentState == GameState.State.PlayerMoving)
                    {
                        GameState.CurrentState = GameState.State.MonstersMoving;
                    }
                }
            }
        }
        else if (GameState.CurrentState == GameState.State.MonstersMoving)
        {
            _controllers.RemoveAll(c => c.Owner == null || !c.Owner.Active);

            foreach (var c in _controllers.FindAll(x => x.Owner.Depth == MapSystem.CurrentDepth))
            {
                if (c.Owner.ID == Player.Owner.ID)
                    continue;

                c.Move(new Vector2Int());
                if (GameState.CurrentState != GameState.State.MonstersMoving)
                    break;
            }

            if (GameState.CurrentState == GameState.State.MonstersMoving)
                GameState.CurrentState = GameState.State.PlayerMoving;
        }
	}

    public static void ReStart(List<Entity> entities)
    {
        _instance._controllers.Clear();
        if (entities != null)
        {
            foreach (var e in entities)
            {
                var c = e.GetComponent<ControllerComponent>(ComponentType.Controller);
                if (c != null)
                {
                    Register(c);
                }
            }
        }
    }

    public static void Register(ControllerComponent c)
    {
        _instance._controllers.Add(c);
    }
}
