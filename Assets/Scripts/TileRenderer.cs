﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRenderer : MonoBehaviour {

    public TextMesh TextMesh;
    public SpriteRenderer Background;

    public void InitTile(RLTile tile)
    {
        if (tile == null) return;

        var display = tile.GetDisplay();
        TextMesh.text = display.Representation.ToString();
        TextMesh.color = display.Color;
        Background.color = display.Background;
        tile.Renderer = this;
    }

    void Awake()
    {
        TextMesh = transform.GetComponent<TextMesh>();
        Background = transform.Find("Background").GetComponent<SpriteRenderer>();
    }
}
